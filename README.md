# Salt-Forge

Salt-Forge is an orchestration and state management tool designed to simplify the deployment and maintenance of cloud infrastructure. Integrated with `salt-cloud` and extended via SaltStack's `boto` modules, it offers support primarily for AWS resources and, with future development, aims to become increasingly cloud-agnostic.

**Note:** While Salt-Forge currently supports AWS for infrastructure builds, it should work for instance builds across other cloud providers, though these remain untested. Plans for future development are detailed below.

## Future Development

Future updates to Salt-Forge will expand its support beyond AWS to support other cloud providers and add functionalities such as:

- Cloud-agnostic infrastructure deployment
- OS-agnostic instance configurations
- Salt-syndic setup for each VPC
- Infrastructure management features, including load balancer, DNS, ECS, and EKS deployment

These additions aim to provide a more comprehensive and multi-cloud-compatible tool for cloud infrastructure orchestration.

---

## Features

- **Salt-Cloud Integration:** Manage cloud instances across multiple providers.
- **AWS Resource Management with Boto Modules:** Supports AWS resources using Salt's boto modules, including `boto_vpc`, `boto_secgroups`, and others. These modules offer essential states and modules for AWS resources and are currently part of Salt's core code but will be moved to Salt-Extensions in an upcoming release.
- **Orchestration:** Streamline complex deployment processes.
- **State Management:** Ensure idempotency and reliability in configuration management.
- **EKS and Kubernetes Management:**
  - Deploy and manage Amazon EKS clusters
  - Handle node groups and worker nodes
  - Basic cluster operations through boto3_eks module
- **Dependency Tracking:**
  - Automatic tracking of resource dependencies
  - Graph-based deployment ordering
  - Dependency validation before execution
- **Custom Modules and States:** Extend functionality with custom modules in `_modules` and custom states in `_states`.
  - **boto3_vpc.py:** Provides execution and state modules for managing AWS VPC resources, including creating, deleting, and managing instances, subnets, route tables, internet gateways, NAT gateways, and security groups within a VPC.
  - **boto_transit_gw.py:** Provides execution and state modules for managing AWS Transit Gateways, including creating and deleting transit gateways, VPC attachments, and transit gateway route tables, as well as managing routes and waiting for resource states.
  - **boto3_eks.py:** Provides execution and state modules for managing AWS EKS clusters, node groups, and related resources.
  - **kube.py:** Manages Kubernetes resources including deployments, services, and configuration.
- **Infrastructure Management:** Supports creating, rebuilding, and destroying AWS resources, including VPCs, subnets, route tables, routes, subnet associations, internet/NAT gateways, transit gateways, instances, and security groups.
  - Kubernetes cluster provisioning and scaling
  - Container orchestration and management
  
## Prerequisites

1. A repository containing the [salt-forge codebase](https://gitlab.com/gadhs/ogg/application/salt-forge.git) that is used as a [GitFS fileserver for Salt](https://docs.saltproject.io/en/latest/topics/tutorials/gitfs.html).
2. A repository containing organization-specific configuration data, which Salt accesses via GitFS pillars.
   - **Salt Pillar:** Salt Pillars store secure data, encrypted as needed, to be distributed to minions. See [Salt Pillar Documentation](https://docs.saltproject.io/en/latest/topics/pillar/index.html).
3. A fresh installation of an OS with Salt-Master support on a machine with at least 8 GB RAM and [GitFS dependencies](https://docs.saltproject.io/en/latest/topics/tutorials/gitfs.html#installing-dependencies). Salt recommends using the `pygit2` library for efficiency in managing Git-based filesystems.

   **Network Requirements:** All minions must be able to reach the Salt Master on TCP 4505/4506 and TCP 8000 (required for Salt API). However, the Salt Master does not need to reach minions on any specific ports.

---

## Getting Started

### Setting Up the Salt Master

Salt Master setup instructions for RHEL are provided below. For other Linux distributions, refer to [SaltStack Installation Documentation](https://docs.saltproject.io/en/latest/topics/installation/index.html) for OS-specific guidance.

#### RHEL based Installation

```bash
#! /bin/bash

## UPDATE REPO AND INSTALL PKGS ##
dnf update -y
dnf install -y \
    https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm \
    https://dl.fedoraproject.org/pub/epel/epel-next-release-latest-9.noarch.rpm
dnf install -y python3-tornado haveged curl python3-pygit2 patchelf python3-pip

## INITIALIZE SALT ##
mkdir -p /srv/salt /srv/dynamic_pillar
curl -L -o /tmp/bootstrap_salt.sh https://bootstrap.saltstack.com
/bin/sh /tmp/bootstrap_salt.sh -M -x python3 -X -i salt-forge
yum install -y salt-api salt-cloud

cat << EOF > /etc/salt/master
file_roots:
  base:
    - /srv/salt/
fileserver_backend:
  - git
  - roots
ext_pillar:
  - git:
    - main https://gitlab.com/<your pillar repo>:
      - env: base
      - user: <user>
      - password: <your-gitlab token>
ext_pillar_first: False
gitfs_remotes:
  - https://gitlab.com/<repo>:
    - saltenv:
      - base
        - ref: main
gitfs_saltenv_whitelist:
  - base
gitfs_update_interval: 120
hash_type: sha512
interface: 0.0.0.0
output_indent: pretty
netapi_enable_clients:
  - local
  - local_async
  - runner_async
  - wheel_async
pillar_roots:
  base:
    - /srv/dynamic_pillar
state_output: changes
top_file_merging_strategy: same
EOF
systemctl restart salt-master

## this version due to a bug in pygit that should be fixed upstream soon.
salt-pip install pygit2==1.13.1

## INITIALIZE GPG KEYS ##
cat << EOF > /root/keygen.conf
Key-Type: eddsa
Key-Curve: Ed25519
Key-Usage: sign
Subkey-Type: ecdh
Subkey-Curve: Curve25519
Subkey-Usage: encrypt
Name-Real: Salt-Forge
Name-Email: salt-forge@org
Expire-Date: 0
%no-protection
%commit
EOF

mkdir -p /etc/salt/gpgkeys
chmod 0700 /etc/salt/gpgkeys
cat /root/keygen.conf | gpg --expert --full-gen-key --homedir /etc/salt/gpgkeys/ --batch
gpg --export --homedir /etc/salt/gpgkeys -a > /root/key.gpg
gpg --export-secret-keys --homedir /etc/salt/gpgkeys -a > /root/seckey.gpg
gpg --import /root/key.gpg
gpg --import seckey.gpg
salt-call --local grains.setval type salt
salt-call --local grains.setval role salt

reboot
```

**Testing Setup:** Once Salt Master is installed, you may verify the installation and configuration by running test commands and syncing all modules, ensuring minions are registered correctly with the master.

### Encrypting Secrets
Use GPG to encrypt sensitive information such as AWS credentials, API tokens, and other secrets stored in the Salt Pillar configuration. Example encryption steps are as follows:

```bash
echo -n <your secret> | gpg -aer Salt-Forge
```

  **Note:** If you do not know the name of your gpg public key you can list them with `gpg -k`

  **Types of Secrets:** Typical secrets to encrypt might include cloud provider credentials, database credentials, and API tokens for third-party integrations. Add encrypted secrets to pillar configuration files where required.

Example:
```yaml
cloud_providers:
  aws:
    aws_access_key_id: |
        -----BEGIN PGP MESSAGE-----

        hF456ESAQdRIWduL6rBAAAkWDxEvrPAwp4oDSNglyW7wVsHm7dKJTV/GBmWX+lIw
        j1I5DMj8TVSkw/lS7x4YwF304Sb7emb+ZNJDZxDviMAWZJRe8y4Vr+zoo2aytG/s
        1FkBCQIQlNDVrqQWSSD7zqetAhmbWJd+8ORINcWMNWEocx3PxsFTEiQwqCxIOkLM
        5b0RqXuAV8OpSibgdGgTYMTKEttm++VLbJmW/ieMs18pOG5x6WFnkno/Wg==
        =A9Hy
        -----END PGP MESSAGE-----
    aws_secret_access_key: |
        -----BEGIN PGP MESSAGE-----

        hFW5AkW46QDxERIdAqrmESAGB/GH+rsekwKbSS5gJJyvN7oVCc2iOUWYUHfhuDsw
        Qi4M/7kGw0z/ELKXCQo09mhx4OoAfylwQyCmgR9J9W37ToprBsTRUjA2/cT46pqQ
        1G0IQBCQpFDYN9eztWnj4qefSvNjsd45CNIKjtUR8GfAPBYzmJBvu2B4r5MOndsI
        t9VQfH+WZ1wQcutB8xjTZXslBpceZnijTKSt8AgHeynVw0e/WK3QYbf0XlZAlerl
        d0a67ZH37XwzLWDqcGGk
        =3ZVz
        -----END PGP MESSAGE-----
    region_name: <region>
```
### Usage
1. **Set Pillar Variables:** Configure key variables for your setup, such as:
  - Organizational data in `environment/answers.sls`, This includes things like your repos, cloud credentials, DNS records, etc.
  - Infrastructure variables in `environment/infra/` This includes VPC's, Routing, Connections, and Security Groups.
  - Instance data in `environment/hosts.sls` This is all the typical data needed to deploy a cloud instance like image1d, subnets, instance tpes, etc.

  **Note:** For additional guidance on pillar variables and examples, refer to the [SaltStack Pillar Walkthrough](https://docs.saltproject.io/en/latest/topics/tutorials/pillar.html) and [SaltStack Pillar Documentation](https://docs.saltproject.io/en/latest/ref/pillar/index.html).

2. **Run Orchestration:** Use the orchestration state to deploy or manage cloud resources. Specify VPC and environment by passing relevant pillar variables to avoid accidental changes:
```bash
salt-run state.orch orch.init pillar='{"vpc":"my-vpc", "env":"blue"}'
```
### Orchestration Process

The orchestration process follows these stages:
**Note:** Each VPC and every host types get deployed in separate runners and the dependency tracking handles when each piece deploys.

1. **VPC Deployment:** Orchestrates vpc.sls in `orch/infra/` based on the data in pillar `environment/infra/vpc.sls` variables.
2. **Transit Gateway Connections:** Establishes connections defined in pillar `environment/infra/vpc.sls#connections` variables.
3. **Security Groups:** Deploys security groups defined in pillar `environment/infra/security_groups.sls.sls` variables.
4. **Deploy EKS Cluster:** Orchestrate the deployment/rebuild/zeroize for AWS EKS Clusters
5. **Manage Node Groups:** Add node groups to EKS for compute resources.
6. **Instance Targeting:** Builds a target list based on pillar `environment/hosts.sls` variables. and orchestrates cloud instance deployments. Supports EC2 and K8s.
7. **Provisioning EC2 Instances:** Completes provisioning based on roles defined in `formulas/common/` and app-specific requirements in `formulas/{{ role }}`.
8. **Deploy k8s resources:** Deploy/rebuild/zeroize k8s resources in EKS i.e. deployments, secrets, services, etc..

### Managing States:
Apply individual states to ensure a state has been applied correctly.

```bash
salt '*' state.apply <path/to/state_name>
```

Highstate an application/host to insure application is configured correctly.

```bash
salt <target> state.highstate
```

### Additional Resources
- **Highstate Meaning:** Learn about highstate, a command that applies the entire configuration across a target system, in [SaltStack Highstate Documentation](https://docs.saltproject.io/en/latest/ref/states/highstatehtml).
- **Salt State Testing:** Use test=true to preview potential changes without making them live.
