"""
This module provides functions to manage AWS VPC resources using boto3.
"""

import time
import logging
import boto3

log = logging.getLogger(__name__)

def __virtual__():
    """
    Ensure the boto3 module is available.
    """
    try:
        import boto3
        return 'boto3_vpc'
    except ImportError:
        return False, 'The boto3 module could not be imported'

def _get_client(
    region=None,
    profile=None
):
    """
    Get a boto3 EC2 client.

    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: boto3 EC2 client.
    """
    session = boto3.Session(profile_name=profile)
    return session.client('ec2', region_name=region)

def get_resource_status(resource_type, resource_id, profile=None, region=None):
    """
    Checks if a resource type is supported and retrieves the resource status using its ID.

    Args:
        resource_type (str): The type of the resource (e.g., "vm", "disk").
        resource_id (str): The ID of the resource.
        profile (str): The AWS profile to use.
        region (str): The AWS region to use.

    Returns:
        str: The status of the resource, or an error message if unsupported.
    """
    # Define supported resource types
    resource_types = ['subnet', 'security_group', 'nat_gateway', 'address', 'route_table']

    # Check if the resource type is supported
    if resource_type not in resource_types:
        return f"Error: Unsupported resource type '{resource_type}'. Supported types are: {', '.join(resource_types)}."

    # Construct the function name for the proxy call
    function_name = f"describe_{resource_type}"

    # Check if the proxy function exists
    if function_name in globals() and callable(globals()[function_name]):
        # Call the proxy function and return its result
        return globals()[function_name](resource_id, profile, region)
    else:
        return f"Error: Proxy function '{function_name}' is not implemented."

def describe_security_group(security_group_id, profile=None, region=None):
    """
    Describe a specific security group by its ID.

    :param security_group_id: ID of the security group.
    :param profile: AWS profile name.
    :param region: AWS region name.
    :return: Dictionary containing the security group details, or None if not found.
    """
    log.info("Describing security group: %s", security_group_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_security_groups(GroupIds=[security_group_id])
        if response['SecurityGroups']:
            return response['SecurityGroups'][0]  # Return the first (and only) security group in the result
        else:
            log.warning("No security group found with ID: %s", security_group_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing security group %s: %s", security_group_id, e)
        return None

def describe_subnet(subnet_id, profile=None, region=None):
    """
    Describe a specific subnet by its ID.

    :param subnet_id: ID of the subnet.
    :param profile: AWS profile name.
    :param region: AWS region name.
    :return: Dictionary containing the subnet details, or None if not found.
    """
    log.info("Describing subnet: %s", subnet_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_subnets(SubnetIds=[subnet_id])
        if response['Subnets']:
            return response['Subnets'][0]  # Return the first (and only) subnet in the result
        else:
            log.warning("No subnet found with ID: %s", subnet_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing subnet %s: %s", subnet_id, e)
        return None

def describe_nat_gateway(nat_gateway_id, profile=None, region=None):
    """
    Describe a specific NAT gateway by its ID.

    :param nat_gateway_id: ID of the NAT gateway.
    :param profile: AWS profile name.
    :param region: AWS region name.
    :return: Dictionary containing the NAT gateway details, or None if not found.
    """
    log.info("Describing NAT gateway: %s", nat_gateway_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_nat_gateways(NatGatewayIds=[nat_gateway_id])
        if response['NatGateways']:
            return response['NatGateways'][0]  # Return the first (and only) NAT gateway in the result
        else:
            log.warning("No NAT gateway found with ID: %s", nat_gateway_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing NAT gateway %s: %s", nat_gateway_id, e)
        return None

def describe_address(allocation_id, profile=None, region=None):
    """
    Describe a specific Elastic IP address by its allocation ID.

    :param allocation_id: Allocation ID of the Elastic IP.
    :param profile: AWS profile name.
    :param region: AWS region name.
    :return: Dictionary containing the Elastic IP details, or None if not found.
    """
    log.info("Describing Elastic IP address: %s", allocation_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_addresses(AllocationIds=[allocation_id])
        if response['Addresses']:
            return response['Addresses'][0]  # Return the first (and only) address in the result
        else:
            log.warning("No Elastic IP address found with allocation ID: %s", allocation_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing Elastic IP address %s: %s", allocation_id, e)
        return None

def describe_route_table(route_table_id, profile=None, region=None):
    """
    Describe a specific route table by its ID.

    :param route_table_id: ID of the route table.
    :param profile: AWS profile name.
    :param region: AWS region name.
    :return: Dictionary containing the route table details, or None if not found.
    """
    log.info("Describing route table: %s", route_table_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_route_tables(RouteTableIds=[route_table_id])
        if response['RouteTables']:
            return response['RouteTables'][0]  # Return the first (and only) route table in the result
        else:
            log.warning("No route table found with ID: %s", route_table_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing route table %s: %s", route_table_id, e)
        return None

def get_vpc_id_by_name(
    vpc_name,
    region=None,
    profile=None
):
    """
    Get the VPC ID by its name.

    :param vpc_name: Name of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: VPC ID or None if not found.
    """
    log.info("Getting VPC ID by name: %s", vpc_name)
    client = _get_client(region, profile)
    response = client.describe_vpcs(
        Filters=[
            {'Name': 'tag:Name', 'Values': [vpc_name]}
        ]
    )
    if response['Vpcs']:
        vpc_id = response['Vpcs'][0]['VpcId']
        log.info("Found VPC ID: %s", vpc_id)
        return vpc_id
    else:
        log.info("No VPC found with name: %s", vpc_name)
        return None

def delete_instances(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all instances in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting instances in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    instances = client.describe_instances(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['Reservations']
    instance_ids = [
        instance['InstanceId']
        for reservation in instances
        for instance in reservation['Instances']
    ]
    if instance_ids:
        client.terminate_instances(InstanceIds=instance_ids)
        for instance in instances:
            _wait_for_instance_termination(
                instance['Instances'][0]['InstanceId'],
                region,
                profile
            )
    log.info("Deleted instances in VPC: %s", vpc_id)

def _wait_for_instance_termination(
    instance_id,
    region=None,
    profile=None,
    timeout=600,
    interval=10
):
    """
    Wait for an instance to terminate.

    :param instance_id: ID of the instance.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param timeout: Maximum wait time in seconds.
    :param interval: Interval between checks in seconds.
    :return: True if the instance terminated, False if timeout.
    """
    log.info("Waiting for instance termination: %s", instance_id)
    client = _get_client(region, profile)
    start_time = time.time()
    while time.time() - start_time < timeout:
        response = client.describe_instances(InstanceIds=[instance_id])
        if response['Reservations'] and response['Reservations'][0]['Instances']:
            state = response['Reservations'][0]['Instances'][0]['State']['Name']
            if state == 'terminated':
                log.info("Instance terminated: %s", instance_id)
                return True
        time.sleep(interval)
    log.warning("Timeout waiting for instance termination: %s", instance_id)
    return False

def delete_internet_gateways(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all internet gateways in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting internet gateways in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    igws = client.describe_internet_gateways(
        Filters=[
            {'Name': 'attachment.vpc-id', 'Values': [vpc_id]}
        ]
    )['InternetGateways']
    for igw in igws:
        client.detach_internet_gateway(InternetGatewayId=igw['InternetGatewayId'], VpcId=vpc_id)
        client.delete_internet_gateway(InternetGatewayId=igw['InternetGatewayId'])
    log.info("Deleted internet gateways in VPC: %s", vpc_id)

def release_unassociated_eips(
    region=None,
    profile=None
):
    """
    Release all unassociated Elastic IP addresses.

    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Releasing unassociated EIPs")
    client = _get_client(region, profile)
    addresses = client.describe_addresses()['Addresses']
    for address in addresses:
        if 'AssociationId' not in address:
            client.release_address(AllocationId=address['AllocationId'])
            log.info("Released EIP: %s", address['PublicIp'])
    log.info("Released all unassociated EIPs")

def delete_nat_gateways(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all NAT gateways in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting NAT gateways in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    nat_gateways = client.describe_nat_gateways(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['NatGateways']
    for nat_gateway in nat_gateways:
        client.delete_nat_gateway(NatGatewayId=nat_gateway['NatGatewayId'])
        _wait_for_nat_gateway_deletion(nat_gateway['NatGatewayId'], region, profile)
    release_unassociated_eips(region, profile)
    log.info("Deleted NAT gateways in VPC: %s", vpc_id)

def _wait_for_nat_gateway_deletion(
    nat_gateway_id,
    region=None,
    profile=None,
    interval=20
):
    """
    Wait for a NAT gateway to be deleted.

    :param nat_gateway_id: ID of the NAT gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param interval: Interval between checks in seconds.
    :return: True if the NAT gateway was deleted.
    """
    log.info("Waiting for NAT gateway deletion: %s", nat_gateway_id)
    client = _get_client(region, profile)
    while True:
        response = client.describe_nat_gateways(NatGatewayIds=[nat_gateway_id])
        state = response['NatGateways'][0]['State']
        if state == 'deleted':
            log.info("NAT gateway deleted: %s", nat_gateway_id)
            return True
        time.sleep(interval)

def delete_subnets(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all subnets in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting subnets in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    subnets = client.describe_subnets(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['Subnets']
    for subnet in subnets:
        client.delete_subnet(SubnetId=subnet['SubnetId'])
    log.info("Deleted subnets in VPC: %s", vpc_id)

def delete_route_tables(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all route tables in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting route tables in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    route_tables = client.describe_route_tables(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['RouteTables']
    for route_table in route_tables:
        if route_table['Associations']:
            for association in route_table['Associations']:
                if not association['Main']:
                    client.disassociate_route_table(
                        AssociationId=association['RouteTableAssociationId']
                    )
        for route in route_table['Routes']:
            if route['DestinationCidrBlock'] != 'local':
                try:
                    client.delete_route(
                        RouteTableId=route_table['RouteTableId'],
                        DestinationCidrBlock=route['DestinationCidrBlock']
                    )
                except Exception as e:
                    log.error(
                        "Failed to delete route %s from route table %s: %s",
                        route['DestinationCidrBlock'],
                        route_table['RouteTableId'],
                        e
                    )
        try:
            client.delete_route_table(RouteTableId=route_table['RouteTableId'])
        except Exception as e:
            log.error(
                "Failed to delete route table %s: %s",
                route_table['RouteTableId'],
                e
            )
    log.info("Deleted route tables in VPC: %s", vpc_id)

def disassociate_route_table(
    association_id,
    region=None,
    profile=None
):
    """
    Disassociate a route table.

    :param association_id: ID of the route table association.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info(
        "Disassociating route table with association ID: %s",
        association_id
    )
    client = _get_client(region, profile)
    try:
        client.disassociate_route_table(AssociationId=association_id)
        log.info(
            "Successfully disassociated route table with association ID: %s",
            association_id
        )
    except Exception as e:
        log.error(
            "Failed to disassociate route table with association ID: %s: %s",
            association_id,
            e
        )

def describe_route_tables(
    vpc_id,
    region=None,
    profile=None
):
    """
    Describe all route tables in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: List of route tables.
    """
    log.info("Describing route tables in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    response = client.describe_route_tables(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )
    return response['RouteTables']

def describe_addresses(
    region=None,
    profile=None
):
    """
    Describe all Elastic IP addresses.

    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: List of Elastic IP addresses.
    """
    log.info("Describing addresses")
    client = _get_client(region, profile)
    response = client.describe_addresses()
    return response['Addresses']

def wait_for_security_group_deletion(client, security_group_id, wait_time=5, max_retries=20):
    """
    Wait for a security group to be successfully deleted.

    :param client: The AWS EC2 client.
    :param security_group_id: The ID of the security group to wait for deletion.
    :param wait_time: Time in seconds to wait between retries.
    :param max_retries: Maximum number of retries before giving up.
    :return: True if the security group is deleted, False otherwise.
    """
    log.info("Waiting for security group %s to be deleted...", security_group_id)
    retries = 0

    while retries < max_retries:
        try:
            # Check if the security group still exists
            client.describe_security_groups(GroupIds=[security_group_id])
            log.info("Security group %s still exists. Retrying in %s seconds...", security_group_id, wait_time)
        except client.exceptions.ClientError as e:
            # If the error indicates the security group no longer exists, we can stop waiting
            if "InvalidGroup.NotFound" in str(e):
                log.info("Security group %s successfully deleted.", security_group_id)
                return True
            else:
                log.error("Unexpected error while waiting for security group deletion: %s", e)
                break

        time.sleep(wait_time)
        retries += 1

    log.warning("Timed out waiting for security group %s to be deleted.", security_group_id)
    return False


def delete_security_groups_and_wait(vpc_id, region=None, profile=None):
    """
    Delete all security groups in a VPC and wait for their successful deletion.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting and waiting for security groups in VPC: %s", vpc_id)
    client = _get_client(region, profile)

    # Get all security groups in the specified VPC
    security_groups = client.describe_security_groups(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['SecurityGroups']

    for security_group in security_groups:
        group_id = security_group['GroupId']
        group_name = security_group['GroupName']

        # Skip the default security group
        if group_name != 'default':
            try:
                log.info("Deleting security group %s (%s)...", group_name, group_id)
                client.delete_security_group(GroupId=group_id)

                # Wait for the security group to be deleted
                if not wait_for_security_group_deletion(client, group_id):
                    log.warning("Failed to confirm deletion of security group %s.", group_id)
            except client.exceptions.ClientError as e:
                log.error("Error deleting security group %s: %s", group_id, e)

    log.info("Finished deleting and waiting for security groups in VPC: %s", vpc_id)

def delete_security_groups(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all security groups in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting security groups in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    security_groups = client.describe_security_groups(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )['SecurityGroups']
    for security_group in security_groups:
        if security_group['GroupName'] != 'default':
            client.delete_security_group(GroupId=security_group['GroupId'])
    log.info("Deleted security groups in VPC: %s", vpc_id)

def describe_security_groups(
    vpc_id,
    region=None,
    profile=None
):
    """
    Describe all security groups in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: List of security groups.
    """
    log.info("Describing security groups in VPC: %s", vpc_id)
    client = _get_client(region, profile)
    response = client.describe_security_groups(
        Filters=[
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )
    return response['SecurityGroups']

def delete_vpc(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting VPC: %s", vpc_id)
    client = _get_client(region, profile)
    client.delete_vpc(VpcId=vpc_id)
    log.info("Deleted VPC: %s", vpc_id)

def delete_route(
    route_table_id,
    destination_cidr_block,
    region=None,
    profile=None
):
    """
    Delete a route from a route table.

    :param route_table_id: ID of the route table.
    :param destination_cidr_block: Destination CIDR block of the route.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info(
        "Deleting route %s from route table %s",
        destination_cidr_block,
        route_table_id
    )
    client = _get_client(region, profile)
    try:
        client.delete_route(
            RouteTableId=route_table_id,
            DestinationCidrBlock=destination_cidr_block
        )
        log.info(
            "Successfully deleted route %s from route table %s",
            destination_cidr_block,
            route_table_id
        )
    except Exception as e:
        log.error(
            "Failed to delete route %s from route table %s: %s",
            destination_cidr_block,
            route_table_id,
            e
        )

def describe_vpc(
    vpc_id,
    region=None,
    profile=None
):
    """
    Describe a single VPC by its ID.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Dictionary containing the VPC details, or None if not found.
    """
    log.info("Describing VPC: %s", vpc_id)
    client = _get_client(region, profile)

    try:
        response = client.describe_vpcs(
            VpcIds=[vpc_id]
        )
        if response['Vpcs']:
            return response['Vpcs'][0]  # Return the first (and only) VPC in the result
        else:
            log.warning("No VPC found with ID: %s", vpc_id)
            return None
    except client.exceptions.ClientError as e:
        log.error("Error describing VPC %s: %s", vpc_id, e)
        return None

def describe_vpcs(
    region=None,
    profile=None
):
    """
    Describe all VPCs.

    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: List of VPCs.
    """
    log.info("Describing VPCs")
    client = _get_client(region, profile)
    response = client.describe_vpcs()
    return response['Vpcs']

def delete_load_balancers(
    vpc_id,
    region=None,
    profile=None
):
    """
    Delete all load balancers and their dependencies in a VPC.

    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info("Deleting load balancers in VPC: %s", vpc_id)
    session = boto3.Session(profile_name=profile, region_name=region)
    elbv2_client = session.client('elbv2')
    elb_client = session.client('elb')

    # Delete Application and Network Load Balancers
    try:
        # First, delete all ALB/NLB load balancers
        load_balancers = elbv2_client.describe_load_balancers().get('LoadBalancers', [])
        if not load_balancers:
            log.info("No ALB/NLB load balancers found in VPC: %s", vpc_id)
        else:
            for lb in load_balancers:
                if lb.get('VpcId') == vpc_id:
                    try:
                        elbv2_client.delete_load_balancer(LoadBalancerArn=lb['LoadBalancerArn'])
                        log.info("Deleted load balancer: %s", lb['LoadBalancerName'])
                        _wait_for_elbv2_deletion(elbv2_client, lb['LoadBalancerArn'])
                    except Exception as e:
                        log.error("Failed to delete load balancer %s: %s", lb['LoadBalancerName'], str(e))

        # Then, clean up target groups
        target_groups = elbv2_client.describe_target_groups().get('TargetGroups', [])
        if not target_groups:
            log.info("No target groups found in VPC: %s", vpc_id)
        else:
            for tg in target_groups:
                if tg.get('VpcId') == vpc_id:
                    try:
                        elbv2_client.delete_target_group(TargetGroupArn=tg['TargetGroupArn'])
                        log.info("Deleted target group: %s", tg['TargetGroupName'])
                    except Exception as e:
                        log.error("Failed to delete target group %s: %s", tg['TargetGroupName'], str(e))

    except client.exceptions.LoadBalancerNotFoundException:
        log.info("No ALB/NLB resources found")
    except Exception as e:
        log.error("Error handling ALB/NLB resources: %s", str(e))

    # Delete Classic Load Balancers
    try:
        load_balancers = elb_client.describe_load_balancers().get('LoadBalancerDescriptions', [])
        if not load_balancers:
            log.info("No Classic load balancers found in VPC: %s", vpc_id)
        else:
            for lb in load_balancers:
                if lb.get('VPCId') == vpc_id:
                    try:
                        elb_client.delete_load_balancer(LoadBalancerName=lb['LoadBalancerName'])
                        log.info("Deleted classic load balancer: %s", lb['LoadBalancerName'])
                        _wait_for_elb_deletion(elb_client, lb['LoadBalancerName'])
                    except Exception as e:
                        log.error("Failed to delete classic load balancer %s: %s", lb['LoadBalancerName'], str(e))
    except client.exceptions.LoadBalancerNotFoundException:
        log.info("No Classic ELB resources found")
    except Exception as e:
        log.error("Error handling Classic ELB resources: %s", str(e))

    log.info("Completed load balancer deletion process for VPC: %s", vpc_id)

def _wait_for_elbv2_deletion(client, lb_arn, timeout=300, interval=10):
    """
    Wait for an ALB/NLB to be deleted.
    """
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            client.describe_load_balancers(LoadBalancerArns=[lb_arn])
            time.sleep(interval)
        except client.exceptions.LoadBalancerNotFoundException:
            return True
    return False

def _wait_for_elb_deletion(client, lb_name, timeout=300, interval=10):
    """
    Wait for a Classic ELB to be deleted.
    """
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            client.describe_load_balancers(LoadBalancerNames=[lb_name])
            time.sleep(interval)
        except client.exceptions.LoadBalancerNotFoundException:
            return True
    return False
