import os
import yaml
from kubernetes import client, config
from kubernetes.client.rest import ApiException
import salt.utils.templates

def __virtual__():
    """
    Returns the module name or False if requirements are not met
    """
    return 'kube'

def _load_kube_config():
    """
    Attempts to load kubernetes configuration from known locations
    Returns: (success: bool, error: str)
    """
    default_locations = [
        '/etc/kubernetes/admin.conf',
        '~/.kube/config'
    ]
    for location in default_locations:
        try:
            if hasattr(config, 'load_kube_config'):
                config.load_kube_config(location)
            else:
                raise AttributeError("module 'kubernetes.config' has no attribute 'load_kube_config'")
            return True, None
        except TypeError as e:
            if "missing 1 required positional argument: 'Loader'" in str(e):
                config.load_kube_config(config_file=location)
                return True, None
            else:
                continue
        except Exception as e:
            continue
    return False, "Failed to load kube config from default locations"

def _get_kube_client(api_type='CoreV1Api'):
    """
    Returns an instance of the specified kubernetes API client
    Args:
        api_type (str): Type of API client to return
    Returns: (client: ApiClient, error: str)
    """
    success, error = _load_kube_config()
    if not success:
        return None, error
    return getattr(client, api_type)(), None

def ping(**kwargs):
    """
    Ping the Kubernetes API server
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.get_api_resources()
        return {'result': True, 'message': 'Successfully connected to the API server'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def pods(namespace='default', **kwargs):
    """
    List all pods in the specified namespace
    Args:
        namespace (str): Namespace to list pods from
    Returns: dict containing:
        result (bool): Success or failure
        pods (list): List of pod names if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        pods = v1.list_namespaced_pod(namespace)
        return {'result': True, 'pods': [pod.metadata.name for pod in pods.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def services(namespace='default', **kwargs):
    """
    List all services in the specified namespace
    Args:
        namespace (str): Namespace to list services from
    Returns: dict containing:
        result (bool): Success or failure
        services (list): List of service names if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        services = v1.list_namespaced_service(namespace)
        return {'result': True, 'services': [svc.metadata.name for svc in services.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def nodes(**kwargs):
    """
    List all nodes in the cluster
    Returns: dict containing:
        result (bool): Success or failure
        nodes (list): List of node names if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        nodes = v1.list_node()
        return {'result': True, 'nodes': [node.metadata.name for node in nodes.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def node(name, **kwargs):
    """
    Get details of a specific node
    Args:
        name (str): Name of the node
    Returns: dict containing:
        result (bool): Success or failure
        node (str): Node name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        node = v1.read_node(name)
        return {'result': True, 'node': node.metadata.name}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def cordon_node(node_name, **kwargs):
    """
    Mark a node as unschedulable
    Args:
        node_name (str): Name of the node to cordon
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    body = {'spec': {'unschedulable': True}}
    try:
        v1.patch_node(node_name, body)
        return {'result': True, 'message': f'Node {node_name} cordoned'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def uncordon_node(node_name, **kwargs):
    """
    Mark a node as schedulable
    Args:
        node_name (str): Name of the node to uncordon
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    body = {'spec': {'unschedulable': False}}
    try:
        v1.patch_node(node_name, body)
        return {'result': True, 'message': f'Node {node_name} uncordoned'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def node_labels(node_name, **kwargs):
    """
    Get labels of a specific node
    Args:
        node_name (str): Name of the node
    Returns: dict containing:
        result (bool): Success or failure
        labels (dict): Node labels if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        node = v1.read_node(node_name)
        return {'result': True, 'labels': node.metadata.labels}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def node_add_label(node_name, label_key, label_value, **kwargs):
    """
    Add a label to a specific node
    Args:
        node_name (str): Name of the node
        label_key (str): Label key
        label_value (str): Label value
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    body = {
        'metadata': {
            'labels': {
                label_key: label_value
            }
        }
    }
    try:
        v1.patch_node(node_name, body)
        return {'result': True, 'message': f'Label {label_key}={label_value} added to node {node_name}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def node_remove_label(node_name, label_key, **kwargs):
    """
    Remove a label from a specific node
    Args:
        node_name (str): Name of the node
        label_key (str): Label key to remove
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        node = v1.read_node(node_name)
        if label_key in node.metadata.labels:
            del node.metadata.labels[label_key]
            body = {
                'metadata': {
                    'labels': node.metadata.labels
                }
            }
            v1.patch_node(node_name, body)
            return {'result': True, 'message': f'Label {label_key} removed from node {node_name}'}
        else:
            return {'result': False, 'error': f'Label {label_key} not found on node {node_name}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def deployments(namespace='default', **kwargs):
    """
    List all deployments in the specified namespace
    Args:
        namespace (str): Namespace to list deployments from
    Returns: dict containing:
        result (bool): Success or failure
        deployments (list): List of deployment names if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}
    try:
        deployments = apps_v1.list_namespaced_deployment(namespace)
        return {'result': True, 'deployments': [deployment.metadata.name for deployment in deployments.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def secrets(namespace='default', **kwargs):
    """
    List all secrets in the specified namespace
    Args:
        namespace (str): Namespace to list secrets from
    Returns: dict containing:
        result (bool): Success or failure
        secrets (list): List of secret names if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        secrets = v1.list_namespaced_secret(namespace)
        return {'result': True, 'secrets': [secret.metadata.name for secret in secrets.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def configmaps(namespace='default', **kwargs):
    """
    List all configmaps in the specified namespace
    Args:
        namespace (str): Namespace to list configmaps from
    Returns: dict containing:
        result (bool): Success or failure
        configmaps (list): List of configmap names if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        configmaps = v1.list_namespaced_config_map(namespace)
        return {'result': True, 'configmaps': [configmap.metadata.name for configmap in configmaps.items]}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_deployment(namespace, name, **kwargs):
    """
    Get details of a specific deployment
    Args:
        namespace (str): Namespace of the deployment
        name (str): Name of the deployment
    Returns: dict containing:
        result (bool): Success or failure
        deployment (str): Deployment name if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}
    try:
        deployment = apps_v1.read_namespaced_deployment(name, namespace)
        if deployment.metadata.name == name:
            return {'result': True, 'deployment': deployment.metadata.name}
        return {'result': False, 'error': 'Deployment not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_service(namespace, name, **kwargs):
    """
    Get details of a specific service
    Args:
        namespace (str): Namespace of the service
        name (str): Name of the service
    Returns: dict containing:
        result (bool): Success or failure
        service (str): Service name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        service = v1.read_namespaced_service(name, namespace)
        if service.metadata.name == name:
            return {'result': True, 'service': service.metadata.name}
        return {'result': False, 'error': 'Service not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_pod(namespace, name, **kwargs):
    """
    Get details of a specific pod
    Args:
        namespace (str): Namespace of the pod
        name (str): Name of the pod
    Returns: dict containing:
        result (bool): Success or failure
        pod (str): Pod name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        pod = v1.read_namespaced_pod(name, namespace)
        if pod.metadata.name == name:
            return {'result': True, 'pod': pod.metadata.name}
        return {'result': False, 'error': 'Pod not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_namespace(name, **kwargs):
    """
    Get details of a specific namespace
    Args:
        name (str): Name of the namespace
    Returns: dict containing:
        result (bool): Success or failure
        namespace (str): Namespace name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        namespace = v1.read_namespace(name)
        if namespace.metadata.name == name:
            return {'result': True, 'namespace': namespace.metadata.name}
        return {'result': False, 'error': 'Namespace not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_secret(namespace, name, **kwargs):
    """
    Get details of a specific secret
    Args:
        namespace (str): Namespace of the secret
        name (str): Name of the secret
    Returns: dict containing:
        result (bool): Success or failure
        secret (str): Secret name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        secret = v1.read_namespaced_secret(name, namespace)
        if secret.metadata.name == name:
            return {'result': True, 'secret': secret.metadata.name}
        return {'result': False, 'error': 'Secret not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def show_configmap(namespace, name, **kwargs):
    """
    Get details of a specific configmap
    Args:
        namespace (str): Namespace of the configmap
        name (str): Name of the configmap
    Returns: dict containing:
        result (bool): Success or failure
        configmap (str): ConfigMap name if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        configmap = v1.read_namespaced_config_map(name, namespace)
        if configmap.metadata.name == name:
            return {'result': True, 'configmap': configmap.metadata.name}
        return {'result': False, 'error': 'ConfigMap not found'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_deployment(namespace, name, **kwargs):
    """
    Delete a specific deployment
    Args:
        namespace (str): Namespace of the deployment
        name (str): Name of the deployment
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}
    try:
        apps_v1.delete_namespaced_deployment(name, namespace)
        return {'result': True, 'message': f'Deployment {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_service(namespace, name, **kwargs):
    """
    Delete a specific service
    Args:
        namespace (str): Namespace of the service
        name (str): Name of the service
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.delete_namespaced_service(name, namespace)
        return {'result': True, 'message': f'Service {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_pod(namespace, name, **kwargs):
    """
    Delete a specific pod
    Args:
        namespace (str): Namespace of the pod
        name (str): Name of the pod
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.delete_namespaced_pod(name, namespace)
        return {'result': True, 'message': f'Pod {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_namespace(name, **kwargs):
    """
    Delete a specific namespace
    Args:
        name (str): Name of the namespace
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.delete_namespace(name)
        return {'result': True, 'message': f'Namespace {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_secret(namespace, name, **kwargs):
    """
    Delete a specific secret
    Args:
        namespace (str): Namespace of the secret
        name (str): Name of the secret
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.delete_namespaced_secret(name, namespace)
        return {'result': True, 'message': f'Secret {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def delete_configmap(namespace, name, **kwargs):
    """
    Delete a specific configmap
    Args:
        namespace (str): Namespace of the configmap
        name (str): Name of the configmap
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    try:
        v1.delete_namespaced_config_map(name, namespace)
        return {'result': True, 'message': f'ConfigMap {name} deleted'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_deployment(namespace, body, template=None, saltenv='base', context=None):
    """
    Create a new deployment in the specified namespace
    Args:
        namespace (str): Namespace to create the deployment in
        body (str): Path to the deployment YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}
    deployment_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        apps_v1.create_namespaced_deployment(namespace, deployment_body)
        return {'result': True, 'message': f'Deployment created in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_service(namespace, body, template=None, saltenv='base', context=None):
    """
    Create a new service in the specified namespace
    Args:
        namespace (str): Namespace to create the service in
        body (str): Path to the service YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    service_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.create_namespaced_service(namespace, service_body)
        return {'result': True, 'message': f'Service created in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_pod(namespace, body, template=None, saltenv='base', context=None):
    """
    Create a new pod in the specified namespace
    Args:
        namespace (str): Namespace to create the pod in
        body (str): Path to the pod YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    pod_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.create_namespaced_pod(namespace, pod_body)
        return {'result': True, 'message': f'Pod created in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_namespace(body, template=None, saltenv='base', context=None):
    """
    Create a new namespace
    Args:
        body (str): Path to the namespace YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    namespace_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.create_namespace(namespace_body)
        return {'result': True, 'message': 'Namespace created'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_secret(namespace, body, template=None, saltenv='base', context=None):
    """
    Create a new secret in the specified namespace
    Args:
        namespace (str): Namespace to create the secret in
        body (str): Path to the secret YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    secret_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.create_namespaced_secret(namespace, secret_body)
        return {'result': True, 'message': f'Secret created in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def create_configmap(namespace, body, template=None, saltenv='base', context=None):
    """
    Create a new configmap in the specified namespace
    Args:
        namespace (str): Namespace to create the configmap in
        body (str): Path to the configmap YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    configmap_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.create_namespaced_config_map(namespace, configmap_body)
        return {'result': True, 'message': f'ConfigMap created in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def replace_deployment(namespace, name, body, template=None, saltenv='base', context=None):
    """
    Replace an existing deployment in the specified namespace
    Args:
        namespace (str): Namespace of the deployment
        name (str): Name of the deployment
        body (str): Path to the deployment YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}
    deployment_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        apps_v1.replace_namespaced_deployment(name, namespace, deployment_body)
        return {'result': True, 'message': f'Deployment {name} replaced in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def replace_service(namespace, name, body, template=None, saltenv='base', context=None):
    """
    Replace an existing service in the specified namespace
    Args:
        namespace (str): Namespace of the service
        name (str): Name of the service
        body (str): Path to the service YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    service_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.replace_namespaced_service(name, namespace, service_body)
        return {'result': True, 'message': f'Service {name} replaced in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def replace_secret(namespace, name, body, template=None, saltenv='base', context=None):
    """
    Replace an existing secret in the specified namespace
    Args:
        namespace (str): Namespace of the secret
        name (str): Name of the secret
        body (str): Path to the secret YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    secret_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.replace_namespaced_secret(name, namespace, secret_body)
        return {'result': True, 'message': f'Secret {name} replaced in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def replace_configmap(namespace, name, body, template=None, saltenv='base', context=None):
    """
    Replace an existing configmap in the specified namespace
    Args:
        namespace (str): Namespace of the configmap
        name (str): Name of the configmap
        body (str): Path to the configmap YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}
    configmap_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)
    try:
        v1.replace_namespaced_config_map(name, namespace, configmap_body)
        return {'result': True, 'message': f'ConfigMap {name} replaced in namespace {namespace}'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def __read_and_render_yaml_file(filepath, template=None, saltenv='base', context=None):
    """
    Read and render a YAML file using Jinja templates
    Args:
        filepath (str): Path to the YAML file
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict: Rendered YAML content
    """
    if filepath.startswith('salt://'):
        cached_file = __salt__['cp.cache_file'](filepath, saltenv)
        if not cached_file:
            raise FileNotFoundError(f'File not found: {filepath}')
        with open(cached_file, 'r') as file:
            template = file.read()
            rendered_template = salt.utils.templates.render_jinja_tmpl(template, {'pillar': __pillar__, 'opts': __opts__, 'salt': __salt__, 'saltenv': saltenv, **(context or {})})
            return yaml.safe_load(rendered_template)
    else:
        with open(filepath, 'r') as file:
            template = file.read()
            rendered_template = salt.utils.templates.render_jinja_tmpl(template, {'pillar': __pillar__, 'opts': __opts__, 'salt': __salt__, 'saltenv': saltenv, **(context or {})})
            return yaml.safe_load(rendered_template)

def apply_blue_green_deployment(namespace, name, body, active_version, promote=False, template=None, saltenv='base', context=None):
    """
    Manages blue-green deployment process
    Args:
        namespace (str): Namespace of the deployment
        name (str): Name of the deployment
        body (str): Path to the deployment YAML file
        active_version (str): Currently active version ('blue' or 'green')
        promote (bool): Whether to promote the new version to active
        template (str): Optional Jinja template to render the YAML file
        saltenv (str): Salt environment
        context (dict): Context for rendering the template
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    apps_v1, error = _get_kube_client('AppsV1Api')
    if not apps_v1:
        return {'result': False, 'error': error}

    target_version = 'green' if active_version == 'blue' else 'blue'
    target_name = f"{name}-{target_version}"
    active_name = f"{name}-{active_version}"

    deployment_body = __read_and_render_yaml_file(body, template=template, saltenv=saltenv, context=context)

    deployment_body['metadata']['name'] = target_name
    deployment_body['spec']['selector']['matchLabels']['version'] = target_version
    deployment_body['spec']['template']['metadata']['labels']['version'] = target_version

    try:
        existing = show_deployment(namespace, target_name)
        if existing['result']:
            apps_v1.replace_namespaced_deployment(target_name, namespace, deployment_body)
        else:
            apps_v1.create_namespaced_deployment(namespace, deployment_body)

        if promote:
            service_name = f"{name}-lb"
            service = show_service(namespace, service_name)
            if service['result']:
                v1, _ = _get_kube_client()
                service_body = v1.read_namespaced_service(service_name, namespace).to_dict()
                service_body['spec']['selector']['version'] = target_version
                v1.replace_namespaced_service(service_name, namespace, service_body)

                existing = show_deployment(namespace, active_name)
                if existing['result']:
                    delete_deployment(namespace, active_name)

            return {'result': True, 'message': f'Blue-green deployment successful, promoted {target_version} to active'}
        return {'result': True, 'message': f'Blue-green deployment successful, {target_version} ready for testing'}
    except ApiException as e:
        return {'result': False, 'error': str(e)}

def switch_deployment_version(namespace, name, target_version, **kwargs):
    """
    Switch traffic between blue/green versions by updating service selectors.
    When stable service is switched to target_version, preview service is switched to the opposite version.
    Args:
        namespace (str): Namespace of the deployment
        name (str): Name of the deployment
        target_version (str): Target version to switch to ('blue' or 'green')
    Returns: dict containing:
        result (bool): Success or failure
        message (str): Success message if successful
        error (str): Error message if failed
    """
    v1, error = _get_kube_client()
    if not v1:
        return {'result': False, 'error': error}

    try:
        stable_name = f"{name}-stable"
        preview_name = f"{name}-preview"
        current_version = 'blue' if target_version == 'green' else 'green'

        if target_version not in ['blue', 'green']:
            return {'result': False, 'error': 'Target version must be either blue or green'}

        try:
            stable_service = v1.read_namespaced_service(stable_name, namespace)
            preview_service = v1.read_namespaced_service(preview_name, namespace)

            if not stable_service or not preview_service:
                return {'result': False, 'error': f'Required services not found: {stable_name} or {preview_name}'}

            if 'app' not in stable_service.spec.selector or 'app' not in preview_service.spec.selector:
                return {'result': False, 'error': 'Services are missing required selector labels'}
        except ApiException as e:
            return {'result': False, 'error': f'Failed to read services: {str(e)}'}

        stable_patch = {
            'spec': {
                'selector': {
                    'app': name,
                    'version': target_version
                }
            }
        }

        preview_patch = {
            'spec': {
                'selector': {
                    'app': name,
                    'version': current_version
                }
            }
        }

        try:
            v1.patch_namespaced_service(stable_name, namespace, stable_patch)
            v1.patch_namespaced_service(preview_name, namespace, preview_patch)
        except ApiException as e:
            return {'result': False, 'error': f'Failed to patch services: {str(e)}'}

        return {
            'result': True,
            'message': f'Successfully switched services: {stable_name} -> {target_version}, {preview_name} -> {current_version}',
            'changes': {
                'stable': target_version,
                'preview': current_version
            }
        }

    except Exception as e:
        return {'result': False, 'error': f'Unexpected error: {str(e)}'}
