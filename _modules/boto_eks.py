import time
import boto3
from botocore.exceptions import ClientError
import logging
from salt.exceptions import CommandExecutionError

log = logging.getLogger(__name__)

def __virtual__():
    return 'boto_eks'

def list_clusters(profile=None):
    """
    List all EKS clusters in the specified region.
    
    :param profile: The name of the AWS profile.
    :return: A list of cluster names.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.list_clusters()
        return response['clusters']
    except ClientError as e:
        print(f"Error listing clusters: {e}")
        return []

def describe_cluster(cluster_name, profile=None):
    """
    Describe an EKS cluster.
    
    :param cluster_name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the cluster details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.describe_cluster(name=cluster_name)
        return {'result': True, 'cluster': response['cluster']}
    except ClientError as e:
        print(f"Error describing cluster: {e}")
        return {'result': False, 'error': str(e)}

def list_nodegroups(cluster_name, profile=None):
    """
    List all nodegroups in an EKS cluster.
    
    :param cluster_name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :return: A list of nodegroup names.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.list_nodegroups(clusterName=cluster_name)
        return response['nodegroups']
    except ClientError as e:
        print(f"Error listing nodegroups: {e}")
        return []

def describe_nodegroup(cluster_name, nodegroup_name, profile=None):
    """
    Describe an EKS nodegroup.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the nodegroup details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.describe_nodegroup(
            clusterName=cluster_name,
            nodegroupName=nodegroup_name
        )
        return response['nodegroup']
    except ClientError as e:
        print(f"Error describing nodegroup: {e}")
        return {}

def list_addons(cluster_name, profile=None):
    """
    List all addons in an EKS cluster.
    
    :param cluster_name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :return: A list of addon names.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.list_addons(clusterName=cluster_name)
        return response['addons']
    except ClientError as e:
        print(f"Error listing addons: {e}")
        return []

def create_cluster(
    cluster_name,
    role_arn,
    resources_vpc_config,
    profile=None,
    logging=None,
    version=None,
    accessConfig=None,
    upgradePolicy=None
):
    """
    Create an EKS cluster.

    :param cluster_name: The name of the EKS cluster.
    :param role_arn: The ARN of the IAM role that provides permissions for the EKS cluster.
    :param resources_vpc_config: The VPC configuration for the EKS cluster.
    :param profile: The name of the AWS profile.
    :param logging: The logging configuration for the cluster.
    :param version: The Kubernetes version for the EKS cluster.
    :param accessConfig: The access configuration for the EKS cluster.
    :param upgradePolicy: The upgrade policy for the EKS cluster.
    :return: A dictionary containing the cluster details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        cluster_params = {
            'name': cluster_name,
            'roleArn': role_arn,
            'resourcesVpcConfig': resources_vpc_config,
            'logging': logging,
            'version': str(version)
        }

        if accessConfig is not None:
            cluster_params['accessConfig'] = accessConfig
        if upgradePolicy is not None:
            cluster_params['upgradePolicy'] = upgradePolicy

        response = eks_client.create_cluster(**cluster_params)
        return response['cluster']
    except ClientError as e:
        print(f"Error creating cluster: {e}")
        return {}

def create_addon(
    cluster_name,
    addon_name,
    addon_version=None,
    profile=None,
    iam_role=None,
    service_account=None
):
    """
    Create an EKS addon.
    
    :param cluster_name: The name of the EKS cluster.
    :param addon_name: The name of the addon.
    :param addon_version: The version of the addon.
    :param profile: The name of the AWS profile.
    :param iam_role: The IAM role for the addon.
    :param service_account: The Kubernetes service account for the addon.
    :return: A dictionary containing the addon details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        addon_params = {
            'clusterName': cluster_name,
            'addonName': addon_name,
            'addonVersion': addon_version
        }

        if iam_role:
            addon_params['serviceAccountRoleArn'] = iam_role
        if service_account:
            addon_params['serviceAccount'] = service_account

        response = eks_client.create_addon(**addon_params)
        return response['addon']
    except ClientError as e:
        print(f"Error creating addon: {e}")
        return {}

def create_nodegroup(
    cluster_name,
    nodegroup_name,
    nodegroup_config,
    profile=None
):
    """
    Create an EKS nodegroup.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param nodegroup_config: The configuration for the nodegroup.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the nodegroup details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.create_nodegroup(
            clusterName=cluster_name,
            nodegroupName=nodegroup_name,
            **nodegroup_config
        )
        return response['nodegroup']
    except ClientError as e:
        print(f"Error creating nodegroup: {e}")
        return {}

def delete_nodegroup(cluster_name, nodegroup_name, profile=None):
    """
    Delete an EKS nodegroup.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param profile: The name of the AWS profile.
    :return: True if the nodegroup was deleted successfully, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        eks_client.delete_nodegroup(
            clusterName=cluster_name,
            nodegroupName=nodegroup_name
        )
        return True
    except ClientError as e:
        print(
            f"Error deleting nodegroup {nodegroup_name} in "
            f"cluster {cluster_name}: {e}"
        )
        return False

def attach_iam_policy(role_name, policy_arn, profile=None):
    """
    Attach an IAM policy to a role.

    :param role_name: The name of the IAM role.
    :param policy_arn: The ARN of the IAM policy.
    :param profile: The name of the AWS profile.
    :return: True if the policy was attached successfully, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    iam_client = session.client('iam')
    try:
        iam_client.attach_role_policy(
            RoleName=role_name,
            PolicyArn=policy_arn
        )
        return True
    except ClientError as e:
        print(f"Error attaching policy {policy_arn} to role {role_name}: {e}")
        return False

def list_attached_policies(role_name, profile=None):
    """
    List all policies attached to a role.

    :param role_name: The name of the IAM role.
    :param profile: The name of the AWS profile.
    :return: A list of policy ARNs.
    """
    session = boto3.Session(profile_name=profile)
    iam_client = session.client('iam')
    try:
        response = iam_client.list_attached_role_policies(RoleName=role_name)
        return [policy['PolicyArn'] for policy in response['AttachedPolicies']]
    except ClientError as e:
        print(f"Error listing policies for role {role_name}: {e}")
        return []

def list_roles(profile=None):
    """
    List all IAM roles.

    :param profile: The name of the AWS profile.
    :return: A list of role names.
    """
    session = boto3.Session(profile_name=profile)
    iam_client = session.client('iam')
    try:
        response = iam_client.list_roles()
        return [role['RoleName'] for role in response['Roles']]
    except ClientError as e:
        print(f"Error listing roles: {e}")
        return []

def upgrade_cluster(cluster_name, version, profile=None):
    """
    Upgrade an EKS cluster to a specified version.

    :param cluster_name: The name of the EKS cluster.
    :param version: The version to upgrade the cluster to.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the upgrade details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.update_cluster_version(
            name=cluster_name,
            version=version
        )
        return response['update']
    except ClientError as e:
        print(f"Error upgrading cluster {cluster_name} to version {version}: {e}")
        return {}

def upgrade_nodegroup(cluster_name, nodegroup_name, version, profile=None):
    """
    Upgrade an EKS nodegroup to a specified version.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param version: The version to upgrade the nodegroup to.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the upgrade details.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    try:
        response = eks_client.update_nodegroup_version(
            clusterName=cluster_name,
            nodegroupName=nodegroup_name,
            version=version
        )
        return response['update']
    except ClientError as e:
        print(
            f"Error upgrading nodegroup {nodegroup_name} in "
            f"cluster {cluster_name} to version {version}: {e}"
        )
        return {}

def wait_for_cluster_ready(cluster_name, profile=None, timeout=600, interval=30):
    """
    Wait for the EKS cluster to be in a ready state.

    :param cluster_name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :param timeout: The maximum time to wait for the cluster to be ready (in seconds).
    :param interval: The interval between status checks (in seconds).
    :return: True if the cluster is ready, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            response = eks_client.describe_cluster(name=cluster_name)
            status = response['cluster']['status']
            if status == 'ACTIVE':
                return True
            elif status in ['FAILED', 'DELETING']:
                print(f"Cluster {cluster_name} is in {status} state.")
                return False
        except ClientError as e:
            print(f"Error describing cluster: {e}")
            return False
        time.sleep(interval)
    print(f"Timeout waiting for cluster {cluster_name} to be ready.")
    return False

def wait_for_nodegroup_ready(
        cluster_name,
        nodegroup_name,
        profile=None,
        timeout=600,
        interval=30
    ):
    """
    Wait for the EKS nodegroup to be in a ready state.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param profile: The name of the AWS profile.
    :param timeout: The maximum time to wait for the nodegroup to be ready (in seconds).
    :param interval: The interval between status checks (in seconds).
    :return: True if the nodegroup is ready, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            response = eks_client.describe_nodegroup(
                clusterName=cluster_name,
                nodegroupName=nodegroup_name
            )
            status = response['nodegroup']['status']
            if status == 'ACTIVE':
                return True
            elif status in ['FAILED', 'DELETING']:
                print(
                    f"Nodegroup {nodegroup_name} in "
                    f"cluster {cluster_name} is in {status} state."
                )
                return False
        except ClientError as e:
            print(f"Error describing nodegroup: {e}")
            return False
        time.sleep(interval)
    print(
        f"Timeout waiting for nodegroup {nodegroup_name} in "
        f"cluster {cluster_name} to be ready.")
    return False

def wait_for_nodegroup_deleted(
        cluster_name,
        nodegroup_name,
        profile=None,
        timeout=600,
        interval=30
    ):
    """
    Wait for the EKS nodegroup to be deleted.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param profile: The name of the AWS profile.
    :param timeout: The maximum time to wait for the nodegroup to be deleted (in seconds).
    :param interval: The interval between status checks (in seconds).
    :return: True if the nodegroup is deleted, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            eks_client.describe_nodegroup(
                clusterName=cluster_name,
                nodegroupName=nodegroup_name
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                return True
            else:
                print(f"Error describing nodegroup: {e}")
                return False
        time.sleep(interval)
    print(
        f"Timeout waiting for nodegroup {nodegroup_name} in "
        f"cluster {cluster_name} to be deleted.")
    return False

def wait_for_cluster_deleted(
        cluster_name,
        profile=None,
        timeout=600,
        interval=30
    ):
    """
    Wait for the EKS cluster to be deleted.

    :param cluster_name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :param timeout: The maximum time to wait for the cluster to be deleted (in seconds).
    :param interval: The interval between status checks (in seconds).
    :return: True if the cluster is deleted, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            eks_client.describe_cluster(name=cluster_name)
        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                return True
            else:
                print(f"Error describing cluster: {e}")
                return False
        time.sleep(interval)
    print(f"Timeout waiting for cluster {cluster_name} to be deleted.")
    return False

def wait_for_update_to_complete(cluster_name, update_id, profile=None, timeout=600, interval=30):
    """
    Wait for the EKS cluster update to complete.

    :param cluster_name: The name of the EKS cluster.
    :param update_id: The ID of the update.
    :param profile: The name of the AWS profile.
    :param timeout: The maximum time to wait for the update to complete (in seconds).
    :param interval: The interval between status checks (in seconds).
    :return: True if the update is complete, otherwise False.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            response = eks_client.describe_update(
                name=cluster_name,
                updateId=update_id
            )
            status = response['update']['status']
            if status == 'Successful':
                return True
            elif status in ['Failed', 'Cancelled']:
                print(f"Update {update_id} for cluster {cluster_name} is in {status} state.")
                return False
        except ClientError as e:
            print(f"Error describing update: {e}")
            return False
        time.sleep(interval)
    print(f"Timeout waiting for update {update_id} for cluster {cluster_name} to complete.")
    return False

def update_cluster_config(cluster_name, profile=None, **kwargs):
    """
    Update the AWS EKS cluster configuration.

    :param cluster_name: The name of the EKS cluster.
    :param profile: The AWS profile to use.
    :param kwargs: Optional configurations like `resources_vpc_config`, `logging`, etc.
    :return: A dictionary containing the update details or an error message.
    """
    session = boto3.Session(profile_name=profile)
    eks_client = session.client('eks')

    allowed_keys = [
        'resources_vpc_config', 'logging', 'accessConfig', 'upgradePolicy',
        'zonalShiftConfig', 'computeConfig', 'kubernetesNetworkConfig', 'storageConfig'
    ]

    # Filter out None values
    updates = {key: kwargs[key] for key in allowed_keys if kwargs.get(key)}

    try:
        current_config = eks_client.describe_cluster(name=cluster_name)['cluster']
        changes = {}
        for key, value in updates.items():
            if current_config.get(key) != value:
                try:
                    response = eks_client.update_cluster_config(name=cluster_name, **{key: value})
                    update_id = response['update']['id']
                    if wait_for_update_to_complete(cluster_name, update_id, profile):
                        changes[key] = response['update']
                    else:
                        raise CommandExecutionError(f"Update {update_id} for {key} in cluster {cluster_name} failed.")
                except ClientError as e:
                    if 'No changes needed' in str(e):
                        log.info(f"No changes needed for {key} in cluster {cluster_name}")
                    else:
                        raise e

        if not changes:
            return {
                'result': True,
                'changes': {},
                'comment': f"Cluster {cluster_name} is already up to date."
            }

        return {
            'result': True,
            'changes': changes,
            'comment': f"Cluster {cluster_name} updated successfully."
        }
    except ClientError as e:
        log.error(f"Error updating cluster config for {cluster_name}: {e}")
        raise CommandExecutionError(f"Failed to update EKS cluster: {e}")
