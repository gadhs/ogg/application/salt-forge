"""
This module provides functions to manage AWS Transit Gateway resources using boto3.
"""

import logging
import time
import boto3

log = logging.getLogger(__name__)

def __virtual__():
    """
    Check if boto3 is available and return the module name.
    """
    try:
        import boto3
        return 'boto_transit_gw'
    except ImportError:
        return False, 'The boto3 module could not be imported'

def _get_client(region=None, profile=None):
    """
    Get a boto3 EC2 client.

    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: boto3 EC2 client.
    """
    session = boto3.Session(profile_name=profile)
    return session.client('ec2', region_name=region)

def create_transit_gateway(
    name,
    description,
    region=None,
    profile=None
):
    """
    Create a new transit gateway.

    :param name: Name of the transit gateway.
    :param description: Description of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Transit gateway response.
    """
    client = _get_client(region, profile)
    response = client.create_transit_gateway(
        Description=description,
        TagSpecifications=[
            {
                'ResourceType': 'transit-gateway',
                'Tags': [
                    {
                        'Key': 'Name',
                        'Value': name
                    }
                ]
            }
        ]
    )
    return response['TransitGateway']

def delete_transit_gateway(
    transit_gateway_id,
    region=None,
    profile=None
):
    """
    Delete a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Delete transit gateway response.
    """
    client = _get_client(region, profile)
    response = client.delete_transit_gateway(
        TransitGatewayId=transit_gateway_id
    )
    return response

def create_vpc_attachment(
    transit_gateway_id,
    vpc_id,
    subnet_ids,
    options=None,
    region=None,
    profile=None
):
    """
    Create a VPC attachment to a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param vpc_id: ID of the VPC.
    :param subnet_ids: List of subnet IDs.
    :param options: Additional options for the attachment.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: VPC attachment response.
    """
    client = _get_client(region, profile)
    params = {
        'TransitGatewayId': transit_gateway_id,
        'VpcId': vpc_id,
        'SubnetIds': subnet_ids
    }
    if options:
        params['Options'] = options
    response = client.create_transit_gateway_vpc_attachment(**params)
    return response['TransitGatewayVpcAttachment']

def delete_vpc_attachment(
    transit_gateway_attachment_id,
    region=None,
    profile=None
):
    """
    Delete a VPC attachment from a transit gateway.

    :param transit_gateway_attachment_id: ID of the transit gateway attachment.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Delete VPC attachment response.
    """
    client = _get_client(region, profile)
    response = client.delete_transit_gateway_vpc_attachment(
        TransitGatewayAttachmentId=transit_gateway_attachment_id
    )
    return response

def create_transit_gateway_route_table(
    transit_gateway_id,
    region=None,
    profile=None
):
    """
    Create a route table for a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Transit gateway route table response.
    """
    client = _get_client(region, profile)
    response = client.create_transit_gateway_route_table(
        TransitGatewayId=transit_gateway_id
    )
    return response['TransitGatewayRouteTable']

def delete_transit_gateway_route_table(
    transit_gateway_route_table_id,
    region=None,
    profile=None
):
    """
    Deletes the specified transit gateway route table.

    :param transit_gateway_route_table_id: The ID of the transit gateway route table to delete.
    :type transit_gateway_route_table_id: str
    :param region: The AWS region to use. If not specified, the default region is used.
    :type region: str, optional
    :param profile: The AWS profile to use. If not specified, the default profile is used.
    :type profile: str, optional
    :return: The response from the delete_transit_gateway_route_table API call.
    :rtype: dict
    """
    client = _get_client(region, profile)
    response = client.delete_transit_gateway_route_table(
        TransitGatewayRouteTableId=transit_gateway_route_table_id
    )
    return response

def create_route(
    transit_gateway_route_table_id,
    destination_cidr_block,
    transit_gateway_attachment_id,
    region=None,
    profile=None
):
    """
    Create a route in a transit gateway route table.

    :param transit_gateway_route_table_id: ID of the transit gateway route table.
    :param destination_cidr_block: Destination CIDR block.
    :param transit_gateway_attachment_id: ID of the transit gateway attachment.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Create route response.
    """
    client = _get_client(region, profile)
    response = client.create_transit_gateway_route(
        TransitGatewayRouteTableId=transit_gateway_route_table_id,
        DestinationCidrBlock=destination_cidr_block,
        TransitGatewayAttachmentId=transit_gateway_attachment_id
    )
    return response

def delete_route(
    transit_gateway_route_table_id,
    destination_cidr_block,
    region=None,
    profile=None
):
    """
    Delete a route from a transit gateway route table.

    :param transit_gateway_route_table_id: ID of the transit gateway route table.
    :param destination_cidr_block: Destination CIDR block.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Delete route response.
    """
    client = _get_client(region, profile)
    response = client.delete_transit_gateway_route(
        TransitGatewayRouteTableId=transit_gateway_route_table_id,
        DestinationCidrBlock=destination_cidr_block
    )
    return response

def transit_gateway_exists(
    transit_gateway_id,
    region=None,
    profile=None
):
    """
    Check if a transit gateway exists.

    :param transit_gateway_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: True if the transit gateway exists, False otherwise.
    """
    client = _get_client(region, profile)
    try:
        response = client.describe_transit_gateways(
            TransitGatewayIds=[transit_gateway_id]
        )
        return len(response['TransitGateways']) > 0
    except client.exceptions.ClientError as e:
        log.error(f"Error checking transit gateway: {e}")
        return False

def vpc_attachment_exists(
    transit_gateway_id,
    vpc_id,
    region=None,
    profile=None
):
    """
    Check if a VPC attachment exists for a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: True if the VPC attachment exists, False otherwise.
    """
    client = _get_client(region, profile)
    try:
        response = client.describe_transit_gateway_vpc_attachments(
            Filters=[
                {'Name': 'transit-gateway-id', 'Values': [transit_gateway_id]},
                {'Name': 'resource-id', 'Values': [vpc_id]},
                {'Name': 'state', 'Values': ['available', 'pending']}
            ]
        )
        return len(response['TransitGatewayVpcAttachments']) > 0
    except client.exceptions.ClientError as e:
        log.error(f"Error checking VPC attachment: {e}")
        return False

def route_exists(
    transit_gateway_route_table_id,
    destination_cidr_block,
    region=None,
    profile=None
):
    """
    Check if a route exists in a transit gateway route table.

    :param transit_gateway_route_table_id: ID of the transit gateway route table.
    :param destination_cidr_block: Destination CIDR block.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: True if the route exists, False otherwise.
    """
    client = _get_client(region, profile)
    try:
        response = client.search_transit_gateway_routes(
            TransitGatewayRouteTableId=transit_gateway_route_table_id,
            Filters=[
                {
                    'Name': 'route.destination-cidr-block',
                    'Values': [destination_cidr_block]
                }
            ]
        )
        return len(response['Routes']) > 0
    except client.exceptions.ClientError as e:
        log.error(f"Error checking route: {e}")
        return False

def get_transit_gateway_id_by_name(
    name,
    region=None,
    profile=None
):
    """
    Get the ID of a transit gateway by its name.

    :param name: Name of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: Transit gateway ID or None if not found.
    """
    client = _get_client(region, profile)
    try:
        response = client.describe_transit_gateways(
            Filters=[
                {
                    'Name': 'tag:Name',
                    'Values': [name]
                }
            ]
        )
        if response['TransitGateways']:
            return response['TransitGateways'][0]['TransitGatewayId']
        else:
            return None
    except client.exceptions.ClientError as e:
        log.error(f"Error getting transit gateway ID by name: {e}")
        return None

def wait_for_transit_gateway(
    transit_gateway_id,
    region=None,
    profile=None,
    timeout=600,
    interval=10
):
    """
    Wait for a transit gateway to become available.

    :param transit_gateway_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param timeout: Timeout in seconds.
    :param interval: Interval in seconds.
    :return: True if the transit gateway becomes available, False otherwise.
    """
    client = _get_client(region, profile)
    start_time = time.time()
    while time.time() - start_time < timeout:
        response = client.describe_transit_gateways(
            TransitGatewayIds=[transit_gateway_id]
        )
        state = response['TransitGateways'][0]['State']
        if state == 'available':
            return True
        elif state in ['deleted', 'deleting']:
            log.error(f"Transit Gateway {transit_gateway_id} is in {state} state.")
            return False
        time.sleep(interval)
    log.error(
        f"Timeout waiting for Transit Gateway {transit_gateway_id} to become available."
    )
    return False

def wait_for_vpc_attachment(
    transit_gateway_attachment_id,
    region=None,
    profile=None,
    timeout=600,
    interval=10
):
    """
    Wait for a VPC attachment to become available.

    :param transit_gateway_attachment_id: ID of the transit gateway attachment.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param timeout: Timeout in seconds.
    :param interval: Interval in seconds.
    :return: True if the VPC attachment becomes available, False otherwise.
    """
    client = _get_client(region, profile)
    start_time = time.time()
    while time.time() - start_time < timeout:
        response = client.describe_transit_gateway_vpc_attachments(
            TransitGatewayAttachmentIds=[transit_gateway_attachment_id]
        )
        state = response['TransitGatewayVpcAttachments'][0]['State']
        if state == 'available':
            return True
        elif state in ['deleted', 'deleting', 'failed']:
            log.error(
                f"VPC Attachment {transit_gateway_attachment_id} is in {state} state."
            )
            return False
        time.sleep(interval)
    log.error(
        f"Timeout waiting for VPC Attachment {transit_gateway_attachment_id} to become available."
    )
    return False

def get_attachment_id_by_name(
    name,
    tg_name,
    vpc_id,
    region=None,
    profile=None
):
    """
    Get the ID of a VPC attachment by its name.

    :param name: Name of the VPC attachment.
    :param tg_name: Name of the transit gateway.
    :param vpc_id: ID of the VPC.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: VPC attachment ID or None if not found.
    """
    client = _get_client(region, profile)
    response = client.describe_transit_gateway_vpc_attachments(
        Filters=[
            {'Name': 'tag:Name', 'Values': [name]},
            {'Name': 'transit-gateway-id', 'Values': [tg_name]},
            {'Name': 'vpc-id', 'Values': [vpc_id]}
        ]
    )
    if response['TransitGatewayVpcAttachments']:
        return response['TransitGatewayVpcAttachments'][0]['TransitGatewayAttachmentId']
    else:
        return None

def wait_for_attachment_deletion(
    attachment_id,
    region=None,
    profile=None,
    timeout=600,
    interval=10
):
    """
    Wait for a VPC attachment to be deleted.

    :param attachment_id: ID of the VPC attachment.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param timeout: Timeout in seconds.
    :param interval: Interval in seconds.
    :return: True if the VPC attachment is deleted, False otherwise.
    """
    client = _get_client(region, profile)
    start_time = time.time()
    while time.time() - start_time < timeout:
        response = client.describe_transit_gateway_vpc_attachments(
            TransitGatewayAttachmentIds=[attachment_id]
        )
        state = response['TransitGatewayVpcAttachments'][0]['State']
        if state == 'deleted':
            return True
        time.sleep(interval)
    return False

def wait_for_transit_gateway_deletion(
    tg_id,
    region=None,
    profile=None,
    timeout=600,
    interval=10
):
    """
    Wait for a transit gateway to be deleted.

    :param tg_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :param timeout: Timeout in seconds.
    :param interval: Interval in seconds.
    :return: True if the transit gateway is deleted, False otherwise.
    """
    client = _get_client(region, profile)
    start_time = time.time()
    while time.time() - start_time < timeout:
        response = client.describe_transit_gateways(TransitGatewayIds=[tg_id])
        state = response['TransitGateways'][0]['State']
        if state == 'deleted':
            return True
        time.sleep(interval)
    return False

def describe_transit_gateway_vpc_attachments(
    transit_gateway_id,
    region=None,
    profile=None
):
    """
    Describe VPC attachments for a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    :return: List of VPC attachments.
    """
    client = _get_client(region, profile)
    response = client.describe_transit_gateway_vpc_attachments(
        Filters=[
            {'Name': 'transit-gateway-id', 'Values': [transit_gateway_id]}
        ]
    )
    return response['TransitGatewayVpcAttachments']

def rename_transit_gateway(
    transit_gateway_id,
    new_name,
    region=None,
    profile=None
):
    """
    Rename a transit gateway.

    :param transit_gateway_id: ID of the transit gateway.
    :param new_name: New name for the transit gateway.
    :param region: AWS region name.
    :param profile: AWS profile name.
    """
    log.info(f"Renaming Transit Gateway {transit_gateway_id} to {new_name}")
    client = _get_client(region, profile)
    try:
        response = client.create_tags(
            Resources=[transit_gateway_id],
            Tags=[{'Key': 'Name', 'Value': new_name}]
        )
        log.info(f"Response from create_tags: {response}")
        log.info(f"Renamed Transit Gateway {transit_gateway_id} to {new_name}")
    except Exception as e:
        log.error(f"Failed to rename Transit Gateway {transit_gateway_id} to {new_name}: {e}")
