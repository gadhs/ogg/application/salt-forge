{% set vpc_name = pillar['vpc'] %}
{% set cluster = salt['pillar.get']('vpcs:' + vpc_name + ':eks_cluster', '') %}

{% if salt['pillar.get']('vpcs:' + vpc_name + ':rebuild', 'False') == True %}
  {% do salt.log.info("VPC: " + vpc_name + " is getting rebuilt.") %}

set_vpc_grain_deploying_{{ vpc_name }}_{{ cluster }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_vpc_{{ vpc_name }}
      - deploying

  {% if cluster %}
    {% do salt.log.info("EKS Cluster: " + cluster + " is getting rebuilt.") %}
set_eks_grain_deploying_{{ vpc_name }}_{{ cluster }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_eks_{{ cluster }}
      - deploying

zeroize_{{ vpc_name }}_{{ cluster }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/states/zeroize_eks
    - pillar:
        cluster: {{ cluster }}
    - queue: True
    - onlyif:
      - salt-call kube.ping

  {% else %}

  test_nop_{{ vpc_name }}:
    test.nop:
      - name: "No EKS cluster associated with VPC: {{ vpc_name }}"

  {% endif %}

zeroize_{{ vpc_name }}_vpc:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/states/zeroize_infra
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - onlyif:
      - salt-call boto_vpc.describe_vpcs profile=aws | grep -q "{{ vpc_name }}"

deploy_vpc_{{ vpc_name }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/vpc
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - require:
      - salt: zeroize_{{ vpc_name }}_vpc

{% do salt.log.info("VPC: " + vpc_name + " connections are getting created.") %}
{{ vpc_name }}_connections:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/connections
    - queue: True
    - require:
      - salt: deploy_vpc_{{ vpc_name }}
    - retry:
        interval: 60
        attempts: 10
        splay: 30

{% do salt.log.info("VPC: " + vpc_name + " routes are getting added.") %}
{{ vpc_name }}_routing_tables:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/routing_tables
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - require:
      - {{ vpc_name }}_connections

{% do salt.log.info("VPC: " + vpc_name + " security groups are getting created.") %}
{{ vpc_name }}_security_groups:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/security_groups
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - require:
      - {{ vpc_name }}_connections

set_vpc_grain_{{ vpc_name }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_vpc_{{ vpc_name }}
      - available
    - require:
      - {{ vpc_name }}_security_groups
      - {{ vpc_name }}_connections
      - {{ vpc_name }}_routing_tables
      - deploy_vpc_{{ vpc_name }}

  {% if cluster %}
    {% do salt.log.info("EKS Cluster: " + cluster + " is getting deployed.") %}
execute_deploy_{{ cluster }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/states/eks_deploy
    - pillar:
        cluster: {{ cluster }}
    - queue: True
    - require:
      - salt: {{ vpc_name }}_security_groups

set_eks_grain_{{ cluster }}_{{ vpc_name }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_eks_{{ cluster }}
      - available
    - require:
      - execute_deploy_{{ cluster }}
  {% else %}

  test_nop_{{ vpc_name }}:
    test.nop:
      - name: "No EKS cluster associated with VPC: {{ vpc_name }}"

  {% endif %}
{% else %}

deploy_vpc_{{ vpc_name }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/vpc
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True

{% do salt.log.info("VPC: " + vpc_name + " connections are getting created.") %}
{{ vpc_name }}_connections:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/connections
    - queue: True
    - require:
      - salt: deploy_vpc_{{ vpc_name }}
    - retry:
        interval: 60
        attempts: 10
        splay: 30

{% do salt.log.info("VPC: " + vpc_name + " routes are getting added.") %}
{{ vpc_name }}_routing_tables:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/routing_tables
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - require:
      - {{ vpc_name }}_connections

{% do salt.log.info("VPC: " + vpc_name + " security groups are getting created.") %}
{{ vpc_name }}_security_groups:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/infra/security_groups
    - pillar:
        vpc: {{ vpc_name }}
    - queue: True
    - require:
      - {{ vpc_name }}_connections

set_vpc_grain_{{ vpc_name }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_vpc_{{ vpc_name }}
      - available
    - require:
      - {{ vpc_name }}_security_groups
      - {{ vpc_name }}_connections
      - {{ vpc_name }}_routing_tables
      - deploy_vpc_{{ vpc_name }}

  {% if cluster %}
    {% do salt.log.info("EKS Cluster: " + cluster + " is getting rebuilt.") %}
execute_deploy_{{ cluster }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch/states/eks_deploy
    - pillar:
        cluster: {{ cluster }}
    - queue: True

set_eks_grain_{{ cluster }}_{{ vpc_name }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - aws_eks_{{ cluster }}
      - available
    - require:
      - execute_deploy_{{ cluster }}
  {% else %}

  test_nop_{{ vpc_name }}:
    test.nop:
      - name: "No EKS cluster associated with VPC: {{ vpc_name }}"

  {% endif %}
{% endif %}