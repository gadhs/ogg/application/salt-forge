{% set type = pillar['type'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}

{% set secrets = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':secrets', {}) %}

{% for name, secret in secrets.items() %}

{{ name }}:
  kube.secret_present:
    - name: {{ name }}
    - namespace: {{ secret.namespace }}
    - body: {{ secret.body }}
    - template: jinja
    - context:
        name: {{ name }}

{% endfor %}
