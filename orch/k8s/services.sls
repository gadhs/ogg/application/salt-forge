{% set type = pillar['type'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}
{% set services = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':services', {}) %}
{% set active_version = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':active_version', 'blue') %}
{% set preview_version = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':preview_version', 'green') %}

{% for name, service in services.items() %}
{{ name }}:
  kube.service_present:
    - name: {{ name }}
    - namespace: {{ service.namespace }}
    - body: {{ service.body }}
    - template: jinja
    - context:
        vpc: {{ vpc }}
        env: {{ env }}
        type: {{ type }}
        name: {{ name }}
        protocol: {{ service.protocol }}
        port: {{ service.port }}
        targetPort: {{ service.targetPort }}
        selector_version: {{ service.selector_version }}
{% endfor %}
