{% set type = pillar['type'] %}
{% set namespace = pillar['namespace'] %}
{% set target_version = pillar['target_version'] %}

switch_version_{{ type }}:
  kube.deployment_switch_version:
    - name: {{ type }}
    - namespace: {{ namespace }}
    - target_version: {{ target_version }}
