{% set type = pillar['type'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}
{% set deployments = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':deployments', {}) %}
{% set active_version = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':active_version', 'blue') %}
{% set deployment_strategy = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':deployment_strategy', 'rolling') %}
{% set registry = salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':image', {}) %}

{% for version, deployment in deployments.items() %}
{{ type }}-{{ version }}:
  kube.deployment_present:
    - name: {{ type }}-{{ version }}
    - namespace: {{ deployment.namespace }}
    - body: {{ deployment.body }}
    - template: jinja
    - context:
        vpc: {{ vpc }}
        env: {{ env }}
        type: {{ type }}
        version: {{ version }}
        deployment_strategy: {{ deployment_strategy }}
        active_version: {{ active_version }}
        url: {{ registry.get(version, {}).get('url', '') }}
        tag: {{ registry.get(version, {}).get('tag', 'latest') }}
        secret: {{ registry.get(version, {}).get('secret', 'gitlab-registry-secret') }}
        tgt_port: {{ registry.get(version, {}).get('tgt_port', 8080) }}
{% endfor %}
