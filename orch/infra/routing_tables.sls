{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% set vpc_name = pillar['vpc'] %}
{% set route_tables = salt['pillar.get']('route_tables:' + vpc_name, {}) %}

{% for rtb_name, rtb in route_tables.items() %}
create_route_table_{{ rtb_name }}:
  module.run:
    - name: boto_vpc.create_route_table
    - kwargs:
      vpc_name: {{ vpc_name }}
      route_table_name: {{ rtb_name }}
      profile: aws
    - unless: salt-call boto_vpc.describe_route_tables vpc_id={{ aws.lookup_vpc_id(vpc_name) }} profile=aws | grep -q {{ rtb_name }}

  {% for route_type, routes in rtb['routes'].items() %}
    {% for route_entry in routes %}
create_route_{{ rtb_name }}-{{ route_type }}:
  module.run:
    - name: boto_vpc.create_route
    - kwargs:
      route_table_name: {{ rtb_name }}
      destination_cidr_block: {{ route_entry.destination_cidr_block }}
      {% if route_entry.gw_type == 'nat_gateway_id' %}
      nat_gateway_id: {{ aws.lookup_nat_gw_id(vpc_name) }}
      {% elif route_entry.gw_type == 'transit_gateway_id' %}
      gateway_id: {{ aws.lookup_transit_gw_id(route_entry.gateway_name) }}
      {% else %}
      {{ route_entry.gw_type }}: {{ route_entry.gateway_name }}
      {% endif %}
      profile: aws
    - watch:
      - module: create_route_table_{{ rtb_name }}
    - unless: salt-call boto_vpc.describe_route_tables vpc_id={{ aws.lookup_vpc_id(vpc_name) }} route_table_name={{ rtb_name }} profile=aws | grep -q {{ route_entry.destination_cidr_block }}
    {% endfor %}
  {% endfor %}

  {% for subnet_name in rtb['subnet_names'] %}
associate_route_table_{{ subnet_name }}:
  module.run:
    - name: boto_vpc.associate_route_table
    - kwargs:
      route_table_name: {{ rtb_name }}
      subnet_name: {{ subnet_name }}
      profile: aws
    - watch:
      - module: create_route_table_{{ rtb_name }}
    - unless: salt-call boto_vpc.describe_route_tables vpc_id={{ aws.lookup_vpc_id(vpc_name) }} profile=aws | grep -q {{ subnet_name }}
  {% endfor %}
{% endfor %}
create_route_salt:
  module.run:
    - name: boto_vpc.replace_route
    - kwargs:
      route_table_name: {{ pillar['salt']['transit_gw']['route_table'] }}
      destination_cidr_block: {{ pillar['salt']['transit_gw']['dest_cidr_block'] }}
      gateway_id: {{ aws.lookup_transit_gw_id(pillar['salt']['transit_gw']['connection_name']) }}
      profile: aws
