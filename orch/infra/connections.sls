{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% for conn_name, connection in pillar['connections'].items() -%}
create_transit_gateway_{{ conn_name }}:
  boto_transit_gw.transit_gw_present:
    - name: {{ connection.name }}
    - description: {{ connection.description }}
    - profile: default

  {%- for vpc_name, vpc in connection.vpcs.items() %}
attach_vpc_{{ vpc_name }}:
  boto_transit_gw.vpc_attach_present:
    - name: {{ connection.name }}-attach-{{ vpc_name }}
    - tg_name: {{ connection.name }}
    - vpc_id: {{ aws.lookup_vpc_id(vpc_name) }}
    - subnet_ids:
    {%- for cidr in vpc.subnets %}
      - {{ aws.lookup_subnet_id(cidr) }}
    {%- endfor %}
    - profile: default
    - require:
      - boto_transit_gw: create_transit_gateway_{{ conn_name }}
  {% endfor %}
{%- endfor %}
