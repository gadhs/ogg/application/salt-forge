{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% set vpc_name = pillar['vpc'] %}
{% set vpc_data = salt['pillar.get']('vpcs:' + vpc_name, {}) %}

{% if salt['pillar.get']('vpcs:' + vpc_name + ':deploy', False) == True %}

create_vpc_{{ vpc_name }}:
  boto_vpc.present:
    - name: {{ vpc_data.name }}
    - cidr_block: {{ vpc_data.cidr_block }}
    - instance_tenancy: {{ vpc_data.instance_tenancy }}
    - dns_support: {{ vpc_data.dns_support }}
    - dns_hostnames: {{ vpc_data.dns_hostnames }}
    - tags: {{ vpc_data.tags }}
    - profile: aws

    {% for subnet in vpc_data['subnets'] %}
create_subnet_{{ subnet.name }}:
  boto_vpc.subnet_present:
    - name: {{ subnet.name }}
    - cidr_block: {{ subnet.cidr_block }}
    - vpc_name: {{ vpc_name }}
    - availability_zone: {{ subnet.availability_zone }}
    {% if 'public' in subnet.name %}
    - auto_assign_public_ipv4: True
    {% endif %}
    - profile: aws
    - require:
      - boto_vpc: create_vpc_{{ vpc_name }}
    {% endfor %}

create_igw_{{ vpc_name }}:
  boto_vpc.internet_gateway_present:
    - name: {{ vpc_name }}-igw
    - vpc_name: {{ vpc_name }}
    - tags:
        name: {{ vpc_name }}-igw
    - profile: aws
    - onlyif: salt-call boto_vpc.describe_vpcs profile=aws | grep -q {{ vpc_name }}

create_nat_gateway_{{ vpc_name }}:
  boto_vpc.nat_gateway_present:
    - name: {{ vpc_name }}-nat
    - subnet_name: {{ vpc_data['subnets'][0].name }}
    - profile: aws
    - require:
      - boto_vpc: create_igw_{{ vpc_name }}
    - watch:
      - boto_vpc: create_igw_{{ vpc_name }}

init_nat_sleep:
  salt.function:
    - name: test.sleep
    - tgt: '{{ pillar['salt']['name'] }}'
    - kwarg:
        length: 30
{% else %}
vpc_not_set_to_deploy:
  test.nop:
    - name: VPC is not set to deploy
{% endif %}
