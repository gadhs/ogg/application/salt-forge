{% set vpc_name = pillar['vpc'] %}
{% set security_groups = salt['pillar.get']('security_groups:' + vpc_name, {}) %}

{% for sg in security_groups %}
create_{{ sg.name }}:
  boto_secgroup.present:
    - name: {{ sg.name }}
    - description: {{ sg.description }}
    - vpc_name: {{ vpc_name }}
    - profile: aws
    {%- if sg.get('rules') %}
    - rules:
      {%- if sg['rules'].get('ingress') %}
        {%- for rule in sg['rules']['ingress'] %}
        - ip_protocol: {{ rule.ip_protocol }}
          from_port: {{ rule.from_port }}
          to_port: {{ rule.to_port }}
          cidr_ip:
            {%- for cidr in rule.cidr_ip %}
            - {{ cidr }}
            {%- endfor %}
        {%- endfor %}
      {%- endif %}
    - rules_egress:
      {%- if sg['rules'].get('egress') %}
        {%- for rule in sg['rules']['egress'] %}
        - ip_protocol: {{ rule.ip_protocol }}
          from_port: {{ rule.from_port }}
          to_port: {{ rule.to_port }}
          cidr_ip:
            {%- if rule.cidr_ip is string %}
            - {{ rule.cidr_ip }}
            {%- else %}
              {%- for cidr in rule.cidr_ip %}
              - {{ cidr }}
              {%- endfor %}
            {%- endif %}
        {%- endfor %}
      {%- endif %}
    {%- endif %}
{% endfor %}