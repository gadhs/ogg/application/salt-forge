{% set type = pillar['type'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}
{% set services = pillar.get('services', {}) %}
{% set deployments = pillar.get('deployments', {}) %}
{% set secrets = pillar.get('secrets', {}) %}

{% for service_name, service_data in services.items() %}
delete_service_{{ service_name }}:
  kube.service_absent:
    - name: '{{ service_name }}'
    - namespace: {{ service_data.namespace }}
{% endfor %}

{% for deployment_name, deployment_data in deployments.items() %}
delete_deployment_{{ deployment_name }}:
  kube.deployment_absent:
    - name: '{{ deployment_name }}'
    - namespace: {{ deployment_data.namespace }}
{% endfor %}

{% for secret_name, secret_data in secrets.items() %}
delete_secret_{{ secret_name }}:
  kube.secret_absent:
    - name: '{{ secret_name }}'
    - namespace: {{ secret_data.namespace }}
{% endfor %}
