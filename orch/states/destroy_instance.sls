{% set type = pillar['type'] %}

{% for key in salt['cmd.shell']('salt-key -L | grep {}'.format(type)).splitlines() %}

zeroize_instance_{{ key }}:
  salt.runner:
    - name: cloud.destroy
    - arg:
        - "{{ key }}"
    - onlyif:
      - salt-key -l acc | grep -q "{{ key }}"

{% endfor %}
