{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% set targets = pillar['targets'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}

{% for id in targets %}
  {% for host_type, host_data in salt['pillar.get']('hosts:' + vpc + ':' + env, {}).items() %}

instance_sleep_{{ host_type }}-{{ targets[id]['uuid'] }}:
  salt.function:
    - name: test.sleep
    - tgt: '{{ pillar['salt']['name'] }}'
    - kwarg:
        length: 10

prepare_instance_{{ host_type }}-{{ targets[id]['uuid'] }}:
  salt.runner:
    - name: cloud.create
    - kwarg:
        provider: {{ host_data['provider'] }}
        instances: {{ host_type }}-{{ targets[id]['uuid'] }}
        driver: {{ host_data['driver'] }}
        size: {{ host_data['size'] }}
        image: {{ host_data['image'] }}
        securitygroupid:
          {%- for secgroup in host_data['securitygroup'] %}
          - {{ aws.lookup_secgroup_id(secgroup) }}
          {%- endfor %}
        ssh_username: {{ host_data['ssh_username'] }}
        private_key: {{ host_data['private_key'] }}
        subnetid: {{ aws.lookup_subnet_id(host_data['cidr']) }}
        tag: {{ host_data['tag'] }}

set_spawning_{{ host_type }}-{{ targets[id]['uuid'] }}:
  salt.function:
    - name: grains.set
    - tgt: '{{ host_type }}-{{ targets[id]['uuid'] }}'
    - arg:
      - spawning
    - kwarg:
          val: {{ targets[id]['spawning'] }}
    - require:
      - prepare_instance_{{ host_type }}-{{ targets[id]['uuid'] }}
  {% endfor %}
{% endfor %}