{% set cluster_name = pillar['cluster'] %}

{% if cluster_name %}
delete_services_{{ cluster_name }}:
  kube.service_absent:
    - name: '*'
    - namespace: default

delete_eks_cluster_{{ cluster_name }}:
  boto_eks.cluster_absent:
    - name: {{ cluster_name }}
    - profile: default
    - require:
      - kube: delete_services_{{ cluster_name }}

delete_conf_files:
  file.absent:
    - names:
      - /etc/salt/k8s/salt-service-account.yaml
      - /etc/kubernetes/admin.conf
    - force: True
    - require:
      - kube: delete_services_{{ cluster_name }}
      - boto_eks: delete_eks_cluster_{{ cluster_name }}

clear_kubeconfig_data:
  file.directory:
    - name: /root/.kube
    - clean: True
    - require:
      - boto_eks: delete_eks_cluster_{{ cluster_name }}

{% else %}
no_cluster_specified:
  test.nop:
    - comment: "No EKS cluster specified for deletion."
{% endif %}
