{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% set cluster_name = pillar['cluster'] %}
{% set cluster_config = salt['pillar.get']('eks_clusters:' + cluster_name, {}) %}

create_eks_cluster_{{ cluster_name }}:
  boto_eks.cluster_present:
    - name: {{ cluster_name }}
    - role_arn: {{ aws.lookup_role_arn(cluster_config.role_arn) }}
    - resources_vpc_config:
        subnetIds:
          {% for cidr in cluster_config.resources_vpc_config.subnet_cidr %}
          - {{ aws.lookup_subnet_id(cidr) }}
          {% endfor %}
        securityGroupIds:
          {% for sg in cluster_config.resources_vpc_config.securityGroups %}
          - {{ aws.lookup_secgroup_id(sg) }}
          {% endfor %}
    - profile: {{ cluster_config.profile }}
    - version: "{{ cluster_config.version }}"
    - logging:
        clusterLogging:
          {% for log in cluster_config.logging.clusterLogging %}
          - types: {{ log.types }}
            enabled: {{ log.enabled }}
          {% endfor %}
    - addons:
      {% for addon in cluster_config.addons %}
      - name: {{ addon.name }}
        version: {{ addon.version }}
        {% if addon.get('iam_role', None) %}
        iam_role: {{ aws.lookup_role_arn(addon.iam_role) }}
        {% endif %}
        {% if addon.get('service_account', None) %}
        service_account: {{ addon.service_account }}
        {% endif %}
      {% endfor %}
    - nodegroups:
      {% for nodegroup in cluster_config.nodegroups %}
      - name: {{ nodegroup.name }}
        config:
          subnets:
            {% for cidr in nodegroup.config.subnet_cidr %}
            - {{ aws.lookup_subnet_id(cidr) }}
            {% endfor %}
          instanceTypes:
            {% for instance in nodegroup.config.instanceTypes %}
            - {{ instance }}
            {% endfor %}
          scalingConfig:
            minSize: {{ nodegroup.config.scalingConfig.minSize }}
            maxSize: {{ nodegroup.config.scalingConfig.maxSize }}
            desiredSize: {{ nodegroup.config.scalingConfig.desiredSize }}
          amiType: {{ nodegroup.config.amiType }}
          nodeRole: {{ aws.lookup_role_arn(nodegroup.config.nodeRole) }}
      {% endfor %}
    - accessConfig:
        authenticationMode: {{ cluster_config.access_config.authenticationMode }}
    - upgradePolicy:
        supportType: {{ cluster_config.update_config.upgradePolicy }}
    - wait_timeout: {{ cluster_config.wait_timeout }}
    - wait_interval: {{ cluster_config.wait_interval }}

update_kubeconfig_{{ cluster_name }}:
  cmd.run:
    - name: aws eks --region us-east-1 update-kubeconfig --name {{ cluster_name }}
    - unless: kubectl get nodes
    - require:
      - boto_eks: create_eks_cluster_{{ cluster_name }}

service_account_yaml_{{ cluster_name }}:
  file.managed:
    - name: /etc/salt/k8s/salt-service-account.yaml
    - source: salt://orch/states/files/salt-service-account.yaml
    - require:
      - cmd: update_kubeconfig_{{ cluster_name }}

create_service_account_{{ cluster_name }}:
  cmd.run:
    - name: kubectl apply -f /etc/salt/k8s/salt-service-account.yaml
    - unless: salt-call kube.ping
    - require:
      - file: service_account_yaml_{{ cluster_name }}

update_conf_{{ cluster_name }}:
  cmd.script:
    - source: salt://orch/states/files/update-conf.sh
    - shell: /bin/bash
    - args: "{{ cluster_name }} {{ cluster_config.region }}"
    - unless: salt-call kube.ping
    - require:
      - cmd: create_service_account_{{ cluster_name }}
