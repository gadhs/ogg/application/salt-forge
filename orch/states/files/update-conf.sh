#! /bin/bash

CLUSTER_NAME=$1
REGION=$2

TOKEN=$(kubectl get secret salt-service-account-token -n default -o jsonpath="{.data.token}" | base64 -d)
CLUSTER_INFO=$(aws eks describe-cluster --name $CLUSTER_NAME --region "$REGION" --query "cluster.{endpoint:endpoint,certificateAuthorityData:certificateAuthority.data,arn:arn}" --output json)
SERVER_ENDPOINT=$(echo $CLUSTER_INFO | jq -r '.endpoint')
CA_CERT=$(echo $CLUSTER_INFO | jq -r '.certificateAuthorityData')
CLUSTER_ARN=$(echo $CLUSTER_INFO | jq -r '.arn')

cat <<EOF > /etc/kubernetes/admin.conf
apiVersion: v1
kind: Config
clusters:
- cluster:
    certificate-authority-data: $CA_CERT
    server: $SERVER_ENDPOINT
  name: $CLUSTER_ARN
contexts:
- context:
    cluster: $CLUSTER_ARN
    user: salt-service-account
  name: salt-context
current-context: salt-context
users:
- name: salt-service-account
  user:
    token: $TOKEN
EOF
