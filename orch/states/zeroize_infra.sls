{% import 'formulas/common/macros/aws.sls' as aws with context %}

{% set vpc = pillar['vpc'] %}

  {% set instance_ids_output = salt['cmd.run']('salt-call --local boto_ec2.find_instances filters="{"vpc-id": "' + aws.lookup_vpc_id(vpc) + '"}" profile=aws') %}
  {% set instance_ids = instance_ids_output.split('\n') | select('match', 'i-') | list %}

  {% for instance_id in instance_ids %}
  delete_ec2_instance_{{ instance_id }}:
    salt.function:
      - name: cloud.destroy
      - tgt: '{{ pillar['salt']['name'] }}'
      - arg:
          - {{ instance_id }}
  {% endfor %}

delete_transit_gateway:
  boto_transit_gw.transit_gw_absent:
    - name: {{ pillar['salt']['transit_gw']['connection_name'] }}
    - profile: default

delete_{{ vpc }}:
  boto3_vpc.absent:
    - name: {{ vpc }}
    - profile: default