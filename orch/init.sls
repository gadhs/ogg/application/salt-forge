{% do salt.log.info("****** Highstate salt-master before proceeding ******") %}

master_setup:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - highstate: true
    - fail_minions:
      - '{{ pillar['salt']['name'] }}'
    - queue: true

{% do salt.log.info("****** Building Orchestration Targets ******") %}

{% for vpc_name, vpc in salt['pillar.get']('vpcs', {}).items() %}
  {% do salt.log.info("Started a runner for VPC: " + vpc_name + " to be deployed.") %}

{{ vpc_name }}_exec_runner_delay:
  salt.function:
    - name: test.sleep
    - tgt: '{{ pillar['salt']['name'] }}'
    - kwarg:
        length: 10

deploy_vpc_{{ vpc_name }}:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/infra
        pillar:
          vpc: {{ vpc_name }}
    - parallel: true
    - require:
      - salt: {{ vpc_name }}_exec_runner_delay

  {% for env, env_data in salt['pillar.get']('hosts:' + vpc_name, {}).items() %}
    {% for host_type, host_data in env_data.items() %}
      {% do salt.log.info("****** Host Type: " + host_type + " is being processed") %}
      {% set role = host_data['role'] %}
      {% set targets = {} %}
      {% for id in range(host_data['count']) %}
        {% set targets = targets|set_dict_key_value(id|string+':spawning', loop.index0) %}
        {% set targets = targets|set_dict_key_value(id|string+':uuid', salt['random.get_str']('64', punctuation=False)|uuid) %}
      {% endfor %}
      {% set type = host_type %}
      {% set needs = salt['pillar.get']('hosts:' + type + ':needs', {}) %}

      {% for id in targets %}
        {% do salt.log.info("****** List Orchestration Targets for Host Type: " + host_type + ' UUID: ' + targets[id]['uuid']) %}
      {% endfor %}

{{ vpc_name }}_{{ env }}_{{ type }}_exec_runner_delay:
  salt.function:
    - name: test.sleep
    - tgt: '{{ pillar['salt']['name'] }}'
    - kwarg:
        length: 10
    - parallel: true

create_{{ vpc_name }}_{{ env }}_{{ type }}_exec_runner:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/waiting_room
        pillar:
          type: {{ type }}
          needs: {{ needs }}
          targets: {{ targets }}
          role: {{ role }}
          vpc: {{ vpc_name }}
          env: {{ env }}
          host_data: {{ host_data }}
    - parallel: true
    - require:
      - salt: {{ vpc_name }}_{{ env }}_{{ type }}_exec_runner_delay

    {% endfor %}
  {% endfor %}
{% endfor %}