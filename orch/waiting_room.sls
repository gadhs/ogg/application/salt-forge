{% set type = pillar['type'] %}
{% set needs = pillar['needs'] %}
{% set targets = pillar['targets'] %}
{% set role = pillar['role'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}
{% set eks_cluster = pillar['eks_cluster'] %}
{% set host_data = pillar['host_data'] %}

{% if host_data.get('enabled', False) == True %}
  {% do salt.log.info("****** Created Execution Runner for: " + type) %}

{{ type }}_phase_check_init:
  salt.runner:
    - name: needs.check_all
    - kwarg:
        needs: {{ needs }}
        type: {{ type }}
        vpc: {{ vpc }}
        env: {{ env }}
    - retry:
        interval: 60
        attempts: 240
        splay: 60

  {% if env == 'k8s' %}
    {% do salt.log.info("****** Running Kubernetes deployment for [ " + type + " ] ******") %}
    {% set deployment_strategy = host_data.get('deployment_strategy', 'rolling') %}
    {% set active_version = host_data.get('active_version', 'blue') %}
    {% set preview_version = host_data.get('preview_version', 'green') %}
    {% set promote = host_data.get('promote', False) %}
    {% set previous_version = salt['grains.get']('k8s_' + type + '_version', 'blue') %}
    {% set version_changed = previous_version != active_version %}

    {% if host_data.get('rebuild', False) == True %}
      {% if deployment_strategy == 'blue-green' %}
zeroize_resources_{{ type }}_{{ preview_version }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls: orch.states.zeroize_resources
    - pillar:
        type: {{ type }}
        vpc: {{ vpc }}
        env: {{ env }}
        version: {{ preview_version }}
        services: {{ host_data.get('services', {}) }}
        deployments: {{ host_data.get('deployments', {}) }}
        secrets: {{ host_data.get('secrets', {}) }}
    - queue: true
    - require:
      - salt: {{ type }}_phase_check_init
      {% else %}
zeroize_resources_{{ type }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls: orch.states.zeroize_resources
    - pillar:
        type: {{ type }}
        vpc: {{ vpc }}
        env: {{ env }}
        services: {{ host_data.get('services', {}) }}
        deployments: {{ host_data.get('deployments', {}) }}
        secrets: {{ host_data.get('secrets', {}) }}
    - queue: true
    - require:
      - salt: {{ type }}_phase_check_init
      {% endif %}
    {% endif %}

deploy_k8s_resources_{{ type }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls:
      - orch.k8s.secrets
      - orch.k8s.deployments
      - orch.k8s.services
    - pillar:
        type: {{ type }}
        vpc: {{ vpc }}
        env: {{ env }}
        deployment_strategy: {{ deployment_strategy }}
        active_version: {{ active_version }}
        preview_version: {{ preview_version }}
    - queue: true
    - require:
      - salt: {{ type }}_phase_check_init
      {% if host_data.get('rebuild', False) == True %}
        {% if deployment_strategy == 'blue-green' %}
      - salt: zeroize_resources_{{ type }}_{{ preview_version }}
        {% else %}
      - salt: zeroize_resources_{{ type }}
        {% endif %}
      {% endif %}
    - retry:
        attempts: 10
        interval: 60
        splay: 10

    {% if deployment_strategy == 'blue-green' and (version_changed) %}
    {% set deployment_namespace = host_data.get('deployments', {}).get(active_version, {}).get('namespace', 'default') %}
switch_deployment_version_{{ type }}:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - sls: orch.k8s.switch_version
    - pillar:
        type: {{ type }}
        namespace: {{ deployment_namespace }}
        target_version: {{ active_version }}
    - require:
      - salt: deploy_k8s_resources_{{ type }}

update_version_grain_{{ type }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - 'k8s_{{ type }}_version'
      - {{ active_version }}
    - require:
      - salt: switch_deployment_version_{{ type }}
    {% endif %}

store_initial_version_{{ type }}:
  salt.function:
    - name: grains.setval
    - tgt: '{{ pillar['salt']['name'] }}'
    - arg:
      - 'k8s_{{ type }}_version'
      - {{ active_version }}
    - require:
      - salt: deploy_k8s_resources_{{ type }}

  {% else %}
    {% do salt.log.info("****** Running deploy for [ " + type + " ] ******") %}

  deploy_{{ type }}:
    salt.state:
      - tgt: '{{ pillar['salt']['name'] }}'
      - sls:
        - orch/states/deploy
      - pillar:
          type: {{ type }}
          targets: {{ targets }}
          vpc: {{ vpc }}
          env: {{ env }}
      - queue: true
      - require:
        - salt: {{ type }}_phase_check_init

  {{ type }}_provision_delay:
    salt.function:
      - name: test.sleep
      - tgt: '{{ pillar['salt']['name'] }}'
      - kwarg:
          length: 20
      - require:
        - salt: deploy_{{ type }}

  {% do salt.log.info("****** Running provision for [ " + type + " ] ******") %}
  provision_{{ type }}:
    salt.runner:
      - name: state.orchestrate
      - kwarg:
          mods: orch/provision
          pillar:
            type: {{ type }}
            targets: {{ targets }}
            role: {{ role }}
            vpc: {{ vpc }}
            env: {{ env }}
      - require:
        - salt: {{ type }}_provision_delay

  {% endif %}
{% else %}
skip_state:
  test.nop:
    - name: "Host is not enabled; skipping execution."
{% endif %}