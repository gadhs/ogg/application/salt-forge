{% import 'formulas/common/macros/orchestration.sls' as orchestration with context %}

{% set type = pillar['type'] %}
{% set targets = pillar['targets'] %}
{% set role = pillar['role'] %}
{% set vpc = pillar['vpc'] %}
{% set env = pillar['env'] %}

{% do salt.log.info("****** Running provision for [ " + type + " ] ******") %}


apply_base_{{ type }}:
  salt.state:
    - tgt:
{% for id in targets %}
      - {{ type }}-{{ targets[id]['uuid'] }}
{% endfor %}
    - tgt_type: list
    - sls:
      - formulas/common/base
    - pillar:
        type: {{ type }}
        role: {{ role }}
    - queue: True
    - timeout: 1200
    - retry:
        interval: 60
        attempts: 5
        splay: 0

### This macro renders to a block if there are unmet dependencies
{{ orchestration.needs_check_one(type=type, phase='install', vpc=vpc) }}

apply_install_{{ type }}:
  salt.state:
    - tgt:
{% for id in targets %}
      - {{ type }}-{{ targets[id]['uuid'] }}
{% endfor %}
    - tgt_type: list
    - sls:
      - formulas/{{ role }}/install
    - queue: True
    - timeout: 1200
    - retry:
        interval: 60
        attempts: 5
        splay: 0
    - require:
      - apply_base_{{ type }}

## This macro updates the build_phase grain and forces a mine update
{{ orchestration.build_phase_update(type=type, targets=targets, phase='install') }}

### This macro renders to a block if there are unmet dependencies
{{ orchestration.needs_check_one(type=type, phase='configure', vpc=vpc) }}

apply_configure_{{ type }}:
  salt.state:
    - tgt:
{% for id in targets %}
      - {{ type }}-{{ targets[id]['uuid'] }}
{% endfor %}
    - tgt_type: list
    - sls:
      - formulas/{{ role }}/configure
    - highstate: True
    - queue: True
    - timeout: 1200
    - retry:
        interval: 10
        attempts: 2
        splay: 0
    - require:
      - apply_install_{{ type }}

{% if salt['pillar.get']('hosts:' + vpc + ':' + env + ':' + type + ':rebuild', 'False') == True %}
## this macro executes a reboot and wait loop
{{ orchestration.reboot_and_wait(type=type, targets=targets, phase='configure') }}
{% endif %}

## This macro updates the build_phase grain and forces a mine update
{{ orchestration.build_phase_update(type=type, targets=targets, phase='configure') }}