{% set type = pillar['type'] %}

{% if salt['pillar.get']('universal', False) == False %}
master_setup:
  salt.state:
    - tgt: '{{ pillar['salt']['name'] }}'
    - highstate: true
    - fail_minions:
      - '{{ pillar['salt']['name'] }}'
    - queue: true
{% endif %}

## Create the special targets dictionary and populate it with the 'id' of the target (either the uuid or the spawning)
## as well as its randomized 'uuid'.
{% set targets = {} %}
{% for id in range(pillar['hosts'][type]['count']) %}
  {% set targets = targets|set_dict_key_value(id|string+':spawning', loop.index0) %}
  {% set targets = targets|set_dict_key_value(id|string+':uuid', salt['random.get_str']('64', punctuation=False)|uuid) %}
{% endfor %}

# type is the type of host (salt, cache, etc.)
# fully configure it

deploy_{{ type }}:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/deploy
        pillar:
          type: {{ type }}
          targets: {{ targets }}

provision_{{ type }}:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/provision
        pillar:
          type: {{ type }}
          targets: {{ targets }}
    - require:
      - deploy_{{ type }}

{% set repo_name = salt['pillar.get']('repo_name', 'default_repo') %}
{% set uuid = salt['random.get_str']('64', punctuation=False) %}
{% set vpc_name = 'vpc-' + uuid %}
{% set env = 'dev' %}
{% set subnets = ['subnet-' + salt['random.get_str']('8', punctuation=False) for _ in range(4)] %}
{% set app_name = 'app-' + salt['random.get_str']('8', punctuation=False) %}

# Store generated values in dynamic pillar
/srv/salt/dynamic_pillar/{{ repo_name }}.sls:
  file.managed:
    - contents: |
        vpc_name: {{ vpc_name }}
        env: {{ env }}
        subnets: {{ subnets }}
        app_name: {{ app_name }}

# Deploy the infrastructure
deploy_{{ vpc_name }}:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/init
        pillar:
          vpc: {{ vpc_name }}
          env: {{ env }}
          subnets: {{ subnets }}
          app_name: {{ app_name }}

# Deploy the application
deploy_{{ app_name }}:
  salt.runner:
    - name: state.orchestrate
    - kwarg:
        mods: orch/init-infra
        pillar:
          vpc: {{ vpc_name }}
          env: {{ env }}
          subnets: {{ subnets }}
          app_name: {{ app_name }}
    - require:
      - deploy_{{ vpc_name }}