import logging
import boto3

log = logging.getLogger(__name__)

__virtualname__ = "needs"

def __virtual__():
    """
    Return virtual name of the module.
    :return: The virtual name of the module.
    """
    return __virtualname__

def check_all(type, needs, vpc, env):
    """
    Check whether or not required services in dependencies exist,
    and then will trigger the orchestration routine.
    """
    ret = {"result": True, "type": type, "comment": []}
    log.info("****** Validating Dependencies For Service [ "+type+" ] in VPC [ "+vpc+" ]")

    needs_list = []
    eks_client = boto3.client('eks')
    try:
        for phase in needs:
            if isinstance(needs[phase], list):
                needs_list.extend(needs[phase])
            else:
                needs_list.append(needs[phase])
        log.info("****** [ "+type+" ] has the following Dependencies: "+str(needs_list))

        for service in needs_list:
            if service == 'aws_vpc':
                vpc_status = __salt__['grains.get']('aws_vpc_' + vpc)
                if vpc_status == 'available':
                    tg_attachment = __salt__['boto_transit_gw.vpc_attachment_exists'](vpc, region=None, profile=None)
                    if not tg_attachment:
                        log.info(f"****** Transit Gateway Attachment for VPC [ {vpc} ] is not Available")
                        ret["result"] = False
                        ret["comment"].append(f"Transit Gateway Attachment for VPC {vpc} is not Available")
                        return ret
                elif vpc_status == 'deploying':
                    log.info(f"****** AWS VPC [ {vpc} ] is currently deploying")
                    ret["result"] = False
                    ret["comment"].append(f"AWS VPC {vpc} is currently deploying")
                    return ret
                else:
                    log.info(f"****** AWS VPC [ {vpc} ] is not Available")
                    ret["result"] = False
                    ret["comment"].append(f"AWS VPC {vpc} is not Available")
                    return ret
            elif service == 'aws_eks' and env == 'k8s':
                eks_status = __salt__['grains.get']('aws_eks_' + vpc)
                if eks_status == 'available':
                    nodegroups = eks_client.list_nodegroups(clusterName=vpc)['nodegroups']
                    all_active = all(
                        eks_client.describe_nodegroup(clusterName=vpc, nodegroupName=nodegroup)['nodegroup']['status'] == 'ACTIVE'
                        for nodegroup in nodegroups
                    )
                    if not all_active:
                        log.info(f"****** AWS EKS Nodegroup is not in ACTIVE state")
                        ret["result"] = False
                        ret["comment"].append(f"AWS EKS Nodegroup is not in ACTIVE state")
                        return ret
                elif eks_status == 'deploying':
                    log.info(f"****** AWS EKS Cluster [ {vpc} ] is currently deploying")
                    ret["result"] = False
                    ret["comment"].append(f"AWS EKS Cluster [ {vpc} ] is currently deploying")
                    return ret
                else:
                    log.info(f"****** AWS EKS Cluster in VPC [ {vpc} ] is not Available")
                    ret["result"] = False
                    ret["comment"].append(f"AWS EKS Cluster in VPC {vpc} is not Available")
                    return ret
            else:
                current_status = __salt__['manage.up'](tgt=service+"*")

                if len(current_status) == 0:
                    log.info("****** Dependent Service [ "+service+" ] is not Available")
                    ret["result"] = False
                    ret["comment"].append("Dependent Service "+service+" is not Available")
                    return ret

        if ret["result"] is True:
            __context__["retcode"] = 0
            ret["comment"] = type+" orchestration routine may proceed, all Dependent Services are Available"
            return ret

    except Exception as exc:
        log.error("Exception encountered: %s", exc)
        return False

def check_one(type, needs, vpc, env):
    """
    Check whether or not dependencies are
    satisfied for a specific type and phase.
    """
    ret = {"result": True, "type": type, "comment": []}
    log.info("****** Validating Dependencies For Service [ "+type+" ] in VPC [ "+vpc+" ]")
    eks_client = boto3.client('eks')
    try:
        for dep in needs:
            if dep == 'aws_vpc':
                vpc_status = __salt__['grains.get']('aws_vpc_' + vpc)
                if vpc_status == 'available':
                    tg_attachment = __salt__['boto_transit_gw.vpc_attachment_exists'](vpc, region=None, profile=None)
                    if not tg_attachment:
                        log.info(f"****** Transit Gateway Attachment for VPC [ {vpc} ] is not Available")
                        ret["result"] = False
                        ret["comment"].append(f"Transit Gateway Attachment for VPC {vpc} is not Available")
                        return ret
                elif vpc_status == 'deploying':
                    log.info(f"****** AWS VPC [ {vpc} ] is currently deploying")
                    ret["result"] = False
                    ret["comment"].append(f"AWS VPC {vpc} is currently deploying")
                    return ret
                else:
                    log.info(f"****** AWS VPC [ {vpc} ] is not Available")
                    ret["result"] = False
                    ret["comment"].append(f"AWS VPC {vpc} is not Available")
                    return ret
            elif dep == 'aws_eks' and env == 'k8s':
                eks_status = __salt__['grains.get']('aws_eks_' + vpc)
                if eks_status == 'available':
                    nodegroups = eks_client.list_nodegroups(clusterName=vpc)['nodegroups']
                    all_active = all(
                        eks_client.describe_nodegroup(clusterName=vpc, nodegroupName=nodegroup)['nodegroup']['status'] == 'ACTIVE'
                        for nodegroup in nodegroups
                    )
                    if not all_active:
                        log.info(f"****** AWS EKS Nodegroup is not in ACTIVE state")
                        ret["result"] = False
                        ret["comment"].append(f"AWS EKS Nodegroup is not in ACTIVE state")
                        return ret
                elif eks_status == 'deploying':
                    log.info(f"****** AWS EKS Cluster [ {vpc} ] is currently deploying")
                    ret["result"] = False
                    ret["comment"].append(f"AWS EKS Cluster [ {vpc} ] is currently deploying")
                    return ret
                else:
                    log.info(f"****** AWS EKS Cluster in VPC [ {vpc} ] is not Available")
                    ret["result"] = False
                    ret["comment"].append(f"AWS EKS Cluster in VPC {vpc} is not Available")
                    return ret
            else:
                current_status = __salt__['mine.get'](tgt='G@role:'+dep, tgt_type='compound', fun='build_phase')

                if len(current_status) == 0:
                    log.info("****** No endpoints of type [ "+dep+" ] available for assessment")
                    ret["result"] = False
                    ret["comment"].append("No endpoints of type "+dep+" available for assessment")
                    return ret

                all_ready = all(
                    current_status[endpoint] == needs[dep]
                    for endpoint in current_status
                )
                if not all_ready:
                    log.info("****** Not all endpoints of type [ "+dep+" ] are ready")
                    ret["result"] = False
                    ret["comment"].append("Not all endpoints of type "+dep+" are ready")
                    return ret

        if ret["result"] is True:
            __context__["retcode"] = 0
            ret["comment"] = type+" orchestration routine may proceed"
            return ret

    except Exception as exc:
        log.error("Exception encountered: %s", exc)
        return False
