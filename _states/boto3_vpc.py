"""
This module provides functions to delete AWS VPC resources using boto3.
"""

import logging

log = logging.getLogger(__name__)

def __virtual__():
    """
    Only load if the boto3_vpc module is available in __salt__.
    """
    if 'boto3_vpc.get_vpc_id_by_name' in __salt__:
        return 'boto3_vpc'
    return False, 'boto3_vpc module could not be loaded'

def absent(
    name,
    region=None,
    profile=None
):
    """
    Ensure the named VPC is absent.
    
    :param name: The name of the VPC.
    :param region: The region of the VPC.
    :param profile: The profile to use.
    :return: A dictionary with the result of the operation.
    """
    log.info("Starting deletion of VPC: %s", name)
    ret = {'name': name, 'result': True, 'changes': {}, 'comment': ''}

    vpc_id = __salt__['boto3_vpc.get_vpc_id_by_name'](
        name,
        region,
        profile
    )
    if not vpc_id:
        ret['comment'] = f'VPC {name} does not exist.'
        log.info(ret['comment'])
        return ret

    if __opts__['test']:
        ret['result'] = None
        ret['comment'] = f'VPC {name} would be deleted.'
        log.info(ret['comment'])
        return ret

    try:
        log.info("Deleting instances in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_instances'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting load balancers in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_load_balancers'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting NAT gateways in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_nat_gateways'](
            vpc_id,
            region,
            profile
        )
        log.info("Releasing unassociated EIPs")
        __salt__['boto3_vpc.release_unassociated_eips'](
            region,
            profile
        )
        log.info("Deleting internet gateways in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_internet_gateways'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting route tables in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_route_tables'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting security groups in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_security_groups'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting subnets in VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_subnets'](
            vpc_id,
            region,
            profile
        )
        log.info("Deleting VPC: %s", vpc_id)
        __salt__['boto3_vpc.delete_vpc'](
            vpc_id,
            region,
            profile
        )
        ret['changes'] = {'old': vpc_id, 'new': None}
        ret['comment'] = f'VPC {name} and all associated resources have been deleted.'
        log.info(ret['comment'])
    except Exception as e:
        ret['result'] = False
        ret['comment'] = f'Failed to delete VPC {name}: {e}'
        log.error(ret['comment'])

    return ret
