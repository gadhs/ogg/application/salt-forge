import logging
import salt.exceptions

log = logging.getLogger(__name__)

def __virtual__():
    return 'kube'

def _handle_result(name, result, changes, comment):
    """
    Helper function to standardize return format
    Args:
        name (str): Name of the state
        result (bool): Whether the state succeeded
        changes (dict): Changes made by the state
        comment (str): Comment about what was done
    Returns:
        dict: Standardized state return dictionary
    """
    if isinstance(changes, dict) and changes.get('result') is False:
        # If the execution module returned an error
        return {
            'name': name,
            'result': False,
            'changes': {},
            'comment': changes.get('error', 'Unknown error occurred')
        }
    return {
        'name': name,
        'result': result,
        'changes': changes,
        'comment': comment
    }

def deployment_present(name, namespace, body, template=None, saltenv='base', context=None):
    """
    Ensures a deployment exists in the specified namespace
    Args:
        name (str): Name of the deployment
        namespace (str): Kubernetes namespace
        body (str): YAML definition of the deployment
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"Deployment present called with name: {name}, namespace: {namespace}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Deployment {name} would be created'}

    deployment_strategy = context.get('deployment_strategy', 'rolling')
    active_version = context.get('active_version')
    promote = context.get('promote', False)

    if deployment_strategy == 'blue-green':
        if not active_version:
            return _handle_result(name, False, {}, 'active_version must be specified for blue-green deployments')

        try:
            result = __salt__['kube.apply_blue_green_deployment'](
                namespace,
                name,
                body,
                active_version,
                promote,
                template=template,
                saltenv=saltenv,
                context=context
            )
            return _handle_result(name, result.get('result', False), result, result.get('message', ''))
        except Exception as e:
            return _handle_result(name, False, {}, str(e))
    else:
        try:
            existing = __salt__['kube.show_deployment'](namespace, name)
            if existing['result']:
                return _handle_result(name, True, {}, f'Deployment {name} already exists')
            result = __salt__['kube.create_deployment'](namespace, body, template=template, saltenv=saltenv, context=context)
            return _handle_result(name, result.get('result', False), result, result.get('message', ''))
        except Exception as e:
            return _handle_result(name, False, {}, str(e))

def deployment_absent(name, namespace, saltenv='base'):
    """
    Ensures a deployment is absent in the specified namespace
    Args:
        name (str): Name of the deployment
        namespace (str): Kubernetes namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Deployment {name} would be deleted'}
    try:
        existing = __salt__['kube.show_deployment'](namespace, name)
        if not existing['result']:
            return {'name': name, 'result': True, 'changes': {}, 'comment': f'Deployment {name} does not exist'}
        result = __salt__['kube.delete_deployment'](namespace, name, saltenv=saltenv)
    except salt.exceptions.CommandExecutionError as e:
        raise
    return {'name': name, 'result': True, 'changes': result, 'comment': f'Deployment {name} deleted'}

def deployment_switch_version(name, namespace, target_version, saltenv='base'):
    """
    Switch traffic between blue/green deployment versions
    Args:
        name (str): Name of the deployment
        namespace (str): Kubernetes namespace
        target_version (str): Target version to switch to
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    ret = {
        'name': name,
        'result': False,
        'changes': {},
        'comment': ''
    }

    if __opts__['test']:
        ret['result'] = None
        ret['comment'] = f'Would switch {name} traffic to {target_version} version'
        return ret

    try:
        result = __salt__['kube.switch_deployment_version'](
            namespace=namespace,
            name=name,
            target_version=target_version
        )

        if result['result']:
            ret['result'] = True
            ret['changes'] = {'version': target_version}
            ret['comment'] = result['message']
        else:
            ret['comment'] = result.get('error', 'Failed to switch versions')

    except Exception as e:
        ret['comment'] = str(e)

    return ret

def secret_present(name, namespace, body, template=None, saltenv='base', context=None):
    """
    Ensures a secret exists in the specified namespace
    Args:
        name (str): Name of the secret
        namespace (str): Kubernetes namespace
        body (str): YAML definition of the secret
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"Secret present called with name: {name}, namespace: {namespace}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Secret {name} would be created'}
    try:
        existing = __salt__['kube.show_secret'](namespace, name)
        if existing['result']:
            return _handle_result(name, True, {}, f'Secret {name} already exists')
        result = __salt__['kube.create_secret'](namespace, body, template=template, saltenv=saltenv, context=context)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def secret_absent(name, namespace, saltenv='base'):
    """
    Ensures a secret is absent in the specified namespace
    Args:
        name (str): Name of the secret
        namespace (str): Kubernetes namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Secret {name} would be deleted'}
    try:
        existing = __salt__['kube.show_secret'](namespace, name)
        if not existing['result']:
            return {'name': name, 'result': True, 'changes': {}, 'comment': f'Secret {name} does not exist'}
        result = __salt__['kube.delete_secret'](namespace, name, saltenv=saltenv)
    except salt.exceptions.CommandExecutionError as e:
        raise
    return {'name': name, 'result': True, 'changes': result, 'comment': f'Secret {name} deleted'}

def pod_present(name, namespace, body, template=None, saltenv='base', context=None):
    """
    Ensures a pod exists in the specified namespace
    Args:
        name (str): Name of the pod
        namespace (str): Kubernetes namespace
        body (str): YAML definition of the pod
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"Pod present called with name: {name}, namespace: {namespace}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Pod {name} would be created'}
    try:
        existing = __salt__['kube.show_pod'](namespace, name)
        if existing['result']:
            return _handle_result(name, True, {}, f'Pod {name} already exists')
        result = __salt__['kube.create_pod'](namespace, body, template=template, saltenv=saltenv, context=context)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def pod_absent(name, namespace, saltenv='base'):
    """
    Ensures a pod is absent in the specified namespace
    Args:
        name (str): Name of the pod
        namespace (str): Kubernetes namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Pod {name} would be deleted'}
    try:
        existing = __salt__['kube.show_pod'](namespace, name)
        if not existing['result']:
            return _handle_result(name, True, {}, f'Pod {name} does not exist')
        result = __salt__['kube.delete_pod'](namespace, name, saltenv=saltenv)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def configmap_present(name, namespace, body, template=None, saltenv='base', context=None):
    """
    Ensures a ConfigMap exists in the specified namespace
    Args:
        name (str): Name of the ConfigMap
        namespace (str): Kubernetes namespace
        body (str): YAML definition of the ConfigMap
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"ConfigMap present called with name: {name}, namespace: {namespace}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'ConfigMap {name} would be created'}
    try:
        existing = __salt__['kube.show_configmap'](namespace, name)
        if existing['result']:
            return _handle_result(name, True, {}, f'ConfigMap {name} already exists')
        result = __salt__['kube.create_configmap'](namespace, body, template=template, saltenv=saltenv, context=context)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def configmap_absent(name, namespace, saltenv='base'):
    """
    Ensures a ConfigMap is absent in the specified namespace
    Args:
        name (str): Name of the ConfigMap
        namespace (str): Kubernetes namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'ConfigMap {name} would be deleted'}
    try:
        existing = __salt__['kube.show_configmap'](namespace, name)
        if not existing['result']:
            return _handle_result(name, True, {}, f'ConfigMap {name} does not exist')
        result = __salt__['kube.delete_configmap'](namespace, name, saltenv=saltenv)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def namespace_present(name, body, template=None, saltenv='base', context=None):
    """
    Ensures a namespace exists
    Args:
        name (str): Name of the namespace
        body (str): YAML definition of the namespace
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"Namespace present called with name: {name}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Namespace {name} would be created'}
    try:
        existing = __salt__['kube.show_namespace'](name)
        if existing['result']:
            return _handle_result(name, True, {}, f'Namespace {name} already exists')
        result = __salt__['kube.create_namespace'](body, template=template, saltenv=saltenv, context=context)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def namespace_absent(name, saltenv='base'):
    """
    Ensures a namespace is absent
    Args:
        name (str): Name of the namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Namespace {name} would be deleted'}
    try:
        existing = __salt__['kube.show_namespace'](name)
        if not existing['result']:
            return _handle_result(name, True, {}, f'Namespace {name} does not exist')
        result = __salt__['kube.delete_namespace'](name, saltenv=saltenv)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def service_present(name, namespace, body, template=None, saltenv='base', context=None):
    """
    Ensures a service exists in the specified namespace
    Args:
        name (str): Name of the service
        namespace (str): Kubernetes namespace
        body (str): YAML definition of the service
        template (str): Optional template name
        saltenv (str): Salt environment
        context (dict): Optional template context
    Returns:
        dict: State return dictionary
    """
    log.debug(f"Service present called with name: {name}, namespace: {namespace}, body: {body}, template: {template}, context: {context}")
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Service {name} would be created'}
    try:
        existing = __salt__['kube.show_service'](namespace, name)
        if existing['result']:
            return _handle_result(name, True, {}, f'Service {name} already exists')
        result = __salt__['kube.create_service'](namespace, body, template=template, saltenv=saltenv, context=context)
        return _handle_result(name, result.get('result', False), result, result.get('message', ''))
    except Exception as e:
        return _handle_result(name, False, {}, str(e))

def service_absent(name, namespace, saltenv='base'):
    """
    Ensures a service is absent in the specified namespace
    Args:
        name (str): Name of the service
        namespace (str): Kubernetes namespace
        saltenv (str): Salt environment
    Returns:
        dict: State return dictionary
    """
    if __opts__.get('test', False):
        return {'name': name, 'result': None, 'changes': {}, 'comment': f'Service {name} would be deleted'}
    try:
        if name == '*':
            services = __salt__['kube.services'](namespace)
            if not services['result']:
                return {'name': name, 'result': False, 'changes': {}, 'comment': services['error']}
            for svc_name in services['services']:
                result = __salt__['kube.delete_service'](namespace, svc_name)
                if not result['result']:
                    return _handle_result(name, False, {}, result['error'])
            return {'name': name, 'result': True, 'changes': {}, 'comment': f'All services in namespace {namespace} deleted'}
        else:
            existing = __salt__['kube.show_service'](namespace, name)
            if not existing['result']:
                return {'name': name, 'result': True, 'changes': {}, 'comment': f'Service {name} does not exist'}
            result = __salt__['kube.delete_service'](namespace, name, saltenv=saltenv)
            return {'name': name, 'result': True, 'changes': result, 'comment': f'Service {name} deleted'}
    except salt.exceptions.CommandExecutionError as e:
        raise
