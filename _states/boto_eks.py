import logging
import boto3
from botocore.exceptions import ClientError
from salt.exceptions import CommandExecutionError

log = logging.getLogger(__name__)

def __virtual__():
    """
    Only load if the boto_eks module is available.
    """
    if 'boto_eks.list_clusters' in __salt__:
        return 'boto_eks'
    return False, 'The boto_eks execution module is not available'

def cluster_present(
    name,
    role_arn,
    resources_vpc_config,
    profile=None,
    logging=None,
    addons=None,
    nodegroups=None,
    wait_timeout=600,
    wait_interval=30,
    version=None,
    accessConfig=None,
    upgradePolicy=None
):
    """
    Ensure the EKS cluster is present.

    :param name: The name of the EKS cluster.
    :param role_arn: The ARN of the IAM role that provides permissions for the EKS cluster.
    :param resources_vpc_config: The VPC configuration for the EKS cluster.
    :param profile: The name of the AWS profile.
    :param logging: The logging configuration for the cluster.
    :param addons: A list of addons to be installed on the cluster.
    :param nodegroups: A list of node groups to be created in the cluster.
    :param wait_timeout: The maximum time to wait for the cluster to be ready (in seconds).
    :param wait_interval: The interval between status checks (in seconds).
    :param version: The Kubernetes version for the EKS cluster.
    :param update_config: The update configuration for the cluster.
    :return: A dictionary containing the state result.
    """
    ret = {'name': name, 'result': None, 'comment': '', 'changes': {}}

    clusters = __salt__['boto_eks.list_clusters'](profile=profile)
    if name in clusters:
        cluster_info = __salt__['boto_eks.describe_cluster'](
            cluster_name=name,
            profile=profile
        )
        if not cluster_info.get('result', False):
            ret['result'] = False
            ret['comment'] = (
                f"Failed to retrieve the current configuration of cluster {name}: {cluster_info.get('error', 'Unknown error')}"
            )
            return ret
        if cluster_info['cluster']['status'] == 'ACTIVE':
            ret['comment'] = (
                f'EKS cluster {name} is already present and active.'
            )
            ret['result'] = True
            return ret
        else:
            ret['result'] = False
            ret['comment'] = (
                f'EKS cluster {name} is present but not active. '
                f'Current status: {cluster_info["cluster"]["status"]}'
            )
            return ret
    else:
        if __opts__['test']:
            ret['comment'] = f'EKS cluster {name} would be created.'
            ret['result'] = None
            return ret

        created_cluster = __salt__['boto_eks.create_cluster'](
            cluster_name=name,
            role_arn=role_arn,
            resources_vpc_config=resources_vpc_config,
            profile=profile,
            logging=logging,
            version=str(version),
            accessConfig=accessConfig,
            upgradePolicy=upgradePolicy
        )

        if created_cluster:
            ret['result'] = True
            ret['comment'] = f'EKS cluster {name} has been created.'
            ret['changes'] = {'old': None, 'new': created_cluster}

            # Wait for the cluster to be ready
            if not __salt__['boto_eks.wait_for_cluster_ready'](
                name,
                profile,
                wait_timeout,
                wait_interval
            ):
                ret['result'] = False
                ret['comment'] = (
                    f'Failed to wait for EKS cluster {name} to be ready.'
                )
                return ret
        else:
            ret['result'] = False
            ret['comment'] = f'Failed to create EKS cluster {name}.'
            return ret

    # Check and create addons if necessary
    if addons:
        for addon in addons:
            addon_name = addon.get('name')
            addon_version = addon.get('version')
            existing_addons = __salt__['boto_eks.list_addons'](
                cluster_name=name,
                profile=profile
            )
            if addon_name not in existing_addons:
                created_addon = __salt__['boto_eks.create_addon'](
                    cluster_name=name,
                    addon_name=addon_name,
                    addon_version=addon_version,
                    profile=profile
                )
                if created_addon:
                    ret['changes'][f'addon_{addon_name}'] = created_addon
                else:
                    ret['result'] = False
                    ret['comment'] += (
                        f' Failed to create addon {addon_name}.'
                    )

    # Check and create nodegroups if necessary
    if nodegroups:
        existing_nodegroups = __salt__['boto_eks.list_nodegroups'](
            cluster_name=name,
            profile=profile
        )
        for nodegroup in nodegroups:
            nodegroup_name = nodegroup.get('name')
            nodegroup_config = nodegroup.get('config')
            if nodegroup_name not in existing_nodegroups:
                created_nodegroup = __salt__['boto_eks.create_nodegroup'](
                    cluster_name=name,
                    nodegroup_name=nodegroup_name,
                    nodegroup_config=nodegroup_config,
                    profile=profile
                )
                if created_nodegroup:
                    ret['changes'][f'nodegroup_{nodegroup_name}'] = created_nodegroup
                    # Wait for the nodegroup to be ready
                    if not __salt__['boto_eks.wait_for_nodegroup_ready'](
                        name,
                        nodegroup_name,
                        profile,
                        wait_timeout,
                        wait_interval
                    ):
                        ret['result'] = False
                        ret['comment'] += (
                            f' Failed to wait for nodegroup {nodegroup_name} to be ready.'
                        )
                        return ret

    if not ret['changes']:
        ret['result'] = True
        ret['comment'] = (
            f'EKS cluster {name} is already present and all '
            f'addons and nodegroups are up to date.'
        )

    return ret

def cluster_absent(name, profile=None, wait_timeout=600, wait_interval=30):
    """
    Ensure the EKS cluster is absent.

    :param name: The name of the EKS cluster.
    :param profile: The name of the AWS profile.
    :param wait_timeout: The maximum time to wait for the cluster to be deleted (in seconds).
    :param wait_interval: The interval between status checks (in seconds).
    :return: A dictionary containing the state result.
    """
    ret = {'name': name, 'result': None, 'comment': '', 'changes': {}}

    clusters = __salt__['boto_eks.list_clusters'](profile=profile)
    if name not in clusters:
        ret['result'] = True
        ret['comment'] = f'EKS cluster {name} is already absent.'
        return ret

    if __opts__['test']:
        ret['comment'] = f'EKS cluster {name} would be deleted.'
        ret['result'] = None
        return ret

    try:
        # Delete node groups first
        nodegroups = __salt__['boto_eks.list_nodegroups'](
            cluster_name=name,
            profile=profile
        )
        for nodegroup in nodegroups:
            __salt__['boto_eks.delete_nodegroup'](
                cluster_name=name,
                nodegroup_name=nodegroup,
                profile=profile
            )
            if not __salt__['boto_eks.wait_for_nodegroup_deleted'](
                name,
                nodegroup,
                profile,
                wait_timeout,
                wait_interval
            ):
                ret['result'] = False
                ret['comment'] = (
                    f'Failed to wait for nodegroup {nodegroup} to be deleted.'
                )
                return ret

        # Delete the cluster
        eks_client = boto3.Session(profile_name=profile).client('eks')
        eks_client.delete_cluster(name=name)

        # Wait for the cluster to be deleted
        if not __salt__['boto_eks.wait_for_cluster_deleted'](
            name,
            profile,
            wait_timeout,
            wait_interval
        ):
            ret['result'] = False
            ret['comment'] = (
                f'Failed to wait for EKS cluster {name} to be deleted.'
            )
            return ret

        ret['result'] = True
        ret['comment'] = (
            f'EKS cluster {name} and its nodegroups have been deleted.'
        )
        ret['changes'] = {'old': name, 'new': None}
    except ClientError as e:
        log.error(f"Failed to delete EKS cluster {name}: {e}")
        ret['result'] = False
        ret['comment'] = f'Failed to delete EKS cluster {name}: {e}'

    return ret

def cluster_upgrade(
    name,
    version,
    profile=None
):
    """
    Ensure the EKS cluster is upgraded to the specified version.

    :param name: The name of the EKS cluster.
    :param version: The version to upgrade the cluster to.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the state result.
    """
    ret = {'name': name, 'result': None, 'comment': '', 'changes': {}}

    if __opts__['test']:
        ret['comment'] = (
            f'EKS cluster {name} would be upgraded to version {version}.'
        )
        ret['result'] = None
        return ret

    upgrade_details = __salt__['boto_eks.upgrade_cluster'](
        cluster_name=name,
        version=version,
        profile=profile
    )

    if upgrade_details:
        ret['result'] = True
        ret['comment'] = (
            f'EKS cluster {name} has been upgraded to version {version}.'
        )
        ret['changes'] = {'old': None, 'new': upgrade_details}
    else:
        ret['result'] = False
        ret['comment'] = (
            f'Failed to upgrade EKS cluster {name} to version {version}.'
        )

    return ret

def nodegroup_upgrade(
    cluster_name,
    nodegroup_name,
    version,
    profile=None
):
    """
    Ensure the EKS nodegroup is upgraded to the specified version.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param version: The version to upgrade the nodegroup to.
    :param profile: The name of the AWS profile.
    :return: A dictionary containing the state result.
    """
    ret = {'name': nodegroup_name, 'result': None, 'comment': '', 'changes': {}}

    if __opts__['test']:
        ret['comment'] = (
            f'EKS nodegroup {nodegroup_name} in cluster {cluster_name} '
            f'would be upgraded to version {version}.'
        )
        ret['result'] = None
        return ret

    upgrade_details = __salt__['boto_eks.upgrade_nodegroup'](
        cluster_name=cluster_name,
        nodegroup_name=nodegroup_name,
        version=version,
        profile=profile
    )

    if upgrade_details:
        ret['result'] = True
        ret['comment'] = (
            f'EKS nodegroup {nodegroup_name} in cluster {cluster_name} '
            f'has been upgraded to version {version}.'
        )
        ret['changes'] = {'old': None, 'new': upgrade_details}
    else:
        ret['result'] = False
        ret['comment'] = (
            f'Failed to upgrade EKS nodegroup {nodegroup_name} in '
            f'cluster {cluster_name} to version {version}.'
        )

    return ret

def cluster_config_updated(name, profile=None, **kwargs):
    """
    Ensure that the AWS EKS cluster configuration is updated.

    :param name: The name of the EKS cluster.
    :param profile: The AWS profile to use.
    :param kwargs: Configuration options like `resources_vpc_config`, `logging`, etc.
    :return: A Salt state dictionary indicating success or failure.
    """
    try:
        current_config = __salt__['boto_eks.describe_cluster'](name, profile=profile)
        if not current_config.get('result', False):
            return {
                'name': name,
                'result': False,
                'changes': {},
                'comment': f"Failed to retrieve the current configuration of cluster {name}: {current_config.get('error', 'Unknown error')}",
            }

        # Compare desired config with current config if necessary
        # For simplicity, assume we always attempt an update
        response = __salt__['boto_eks.update_cluster_config'](name, profile=profile, **kwargs)

        if response['result']:
            return {
                'name': name,
                'result': True,
                'changes': response['changes'],
                'comment': f"Cluster {name} configuration updated successfully."
            }
        else:
            return {
                'name': name,
                'result': False,
                'changes': {},
                'comment': f"Cluster {name} update failed: {response.get('comment', 'Unknown error')}"
            }
    except CommandExecutionError as e:
        log.error(f"Error in state module: {e}")
        return {
            'name': name,
            'result': False,
            'changes': {},
            'comment': f"Exception occurred: {e}"
        }

def nodegroup_present(
    cluster_name,
    nodegroup_name,
    nodegroup_config,
    profile=None,
    wait_timeout=600,
    wait_interval=30
):
    """
    Ensure the EKS nodegroup is present.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param nodegroup_config: The configuration for the nodegroup.
    :param profile: The name of the AWS profile.
    :param wait_timeout: The maximum time to wait for the nodegroup to be ready (in seconds).
    :param wait_interval: The interval between status checks (in seconds).
    :return: A dictionary containing the state result.
    """
    ret = {'name': nodegroup_name, 'result': None, 'comment': '', 'changes': {}}

    existing_nodegroups = __salt__['boto_eks.list_nodegroups'](
        cluster_name=cluster_name,
        profile=profile
    )
    if nodegroup_name in existing_nodegroups:
        ret['comment'] = f'Nodegroup {nodegroup_name} is already present in cluster {cluster_name}.'
        ret['result'] = True
        return ret

    if __opts__['test']:
        ret['comment'] = f'Nodegroup {nodegroup_name} would be created in cluster {cluster_name}.'
        ret['result'] = None
        return ret

    created_nodegroup = __salt__['boto_eks.create_nodegroup'](
        cluster_name=cluster_name,
        nodegroup_name=nodegroup_name,
        nodegroup_config=nodegroup_config,
        profile=profile
    )

    if created_nodegroup:
        ret['result'] = True
        ret['comment'] = f'Nodegroup {nodegroup_name} has been created in cluster {cluster_name}.'
        ret['changes'] = {'old': None, 'new': created_nodegroup}

        # Wait for the nodegroup to be ready
        if not __salt__['boto_eks.wait_for_nodegroup_ready'](
            cluster_name,
            nodegroup_name,
            profile,
            wait_timeout,
            wait_interval
        ):
            ret['result'] = False
            ret['comment'] = f'Failed to wait for nodegroup {nodegroup_name} to be ready in cluster {cluster_name}.'
            return ret
    else:
        ret['result'] = False
        ret['comment'] = f'Failed to create nodegroup {nodegroup_name} in cluster {cluster_name}.'
        return ret

    return ret

def nodegroup_absent(
    cluster_name,
    nodegroup_name,
    profile=None,
    wait_timeout=600,
    wait_interval=30
):
    """
    Ensure the EKS nodegroup is absent.

    :param cluster_name: The name of the EKS cluster.
    :param nodegroup_name: The name of the EKS nodegroup.
    :param profile: The name of the AWS profile.
    :param wait_timeout: The maximum time to wait for the nodegroup to be deleted (in seconds).
    :param wait_interval: The interval between status checks (in seconds).
    :return: A dictionary containing the state result.
    """
    ret = {'name': nodegroup_name, 'result': None, 'comment': '', 'changes': {}}

    existing_nodegroups = __salt__['boto_eks.list_nodegroups'](
        cluster_name=cluster_name,
        profile=profile
    )
    if nodegroup_name not in existing_nodegroups:
        ret['comment'] = f'Nodegroup {nodegroup_name} is already absent in cluster {cluster_name}.'
        ret['result'] = True
        return ret

    if __opts__['test']:
        ret['comment'] = f'Nodegroup {nodegroup_name} would be deleted in cluster {cluster_name}.'
        ret['result'] = None
        return ret

    deleted_nodegroup = __salt__['boto_eks.delete_nodegroup'](
        cluster_name=cluster_name,
        nodegroup_name=nodegroup_name,
        profile=profile
    )

    if deleted_nodegroup:
        ret['result'] = True
        ret['comment'] = f'Nodegroup {nodegroup_name} has been deleted in cluster {cluster_name}.'
        ret['changes'] = {'old': nodegroup_name, 'new': None}

        # Wait for the nodegroup to be deleted
        if not __salt__['boto_eks.wait_for_nodegroup_deleted'](
            cluster_name,
            nodegroup_name,
            profile,
            wait_timeout,
            wait_interval
        ):
            ret['result'] = False
            ret['comment'] = f'Failed to wait for nodegroup {nodegroup_name} to be deleted in cluster {cluster_name}.'
            return ret
    else:
        ret['result'] = False
        ret['comment'] = f'Failed to delete nodegroup {nodegroup_name} in cluster {cluster_name}.'
        return ret

    return ret
