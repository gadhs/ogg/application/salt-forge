"""
This module provides functions to manage AWS Transit Gateway resources using boto3.
"""

import logging
import salt.utils.dictupdate as dictupdate
from botocore.exceptions import ClientError

log = logging.getLogger(__name__)

def __virtual__():
    """
    Only load if the boto_transit_gw module is available.
    """
    try:
        import boto3
        return 'boto_transit_gw'
    except ImportError:
        return False, 'The boto3 module could not be imported'

def transit_gw_present(
    name,
    description,
    region=None,
    profile=None
):
    """
    Ensure the specified Transit Gateway is present.
    """
    ret = {'name': name, 'result': True, 'changes': {}, 'comment': ''}

    tg_id = __salt__['boto_transit_gw.get_transit_gateway_id_by_name'](
        name,
        region,
        profile
    )
    if tg_id:
        ret['comment'] = f'Transit Gateway {name} already exists.'
    else:
        if __opts__['test']:
            ret['result'] = None
            ret['comment'] = f'Transit Gateway {name} would be created.'
            return ret

        result = __salt__['boto_transit_gw.create_transit_gateway'](
            name,
            description,
            region,
            profile
        )
        if result:
            tg_id = result['TransitGatewayId']
            ret['changes'] = {'old': None, 'new': result}
            ret['comment'] = f'Transit Gateway {name} created.'
        else:
            ret['result'] = False
            ret['comment'] = f'Failed to create Transit Gateway {name}.'
            return ret

    if not __salt__['boto_transit_gw.wait_for_transit_gateway'](
        tg_id,
        region,
        profile
    ):
        ret['result'] = False
        ret['comment'] = f'Transit Gateway {name} did not become available.'
        return ret

    return ret

def vpc_attach_present(
    name,
    tg_name,
    vpc_id,
    subnet_ids,
    region=None,
    profile=None
):
    """
    Ensure the specified VPC Attachment is present.
    """
    ret = {'name': name, 'result': True, 'changes': {}, 'comment': ''}

    tg_id = __salt__['boto_transit_gw.get_transit_gateway_id_by_name'](
        tg_name,
        region,
        profile
    )
    if not tg_id:
        ret['result'] = False
        ret['comment'] = f'Transit Gateway {tg_name} does not exist.'
        return ret

    if __salt__['boto_transit_gw.vpc_attachment_exists'](
        tg_id,
        vpc_id,
        region,
        profile
    ):
        ret['comment'] = f'VPC Attachment {name} already exists.'
        return ret
    else:
        if __opts__['test']:
            ret['result'] = None
            ret['comment'] = f'VPC Attachment {name} would be created.'
            return ret

        options = {
            'DnsSupport': 'enable',
            'Ipv6Support': 'disable',
            'ApplianceModeSupport': 'disable'
        }
        try:
            result = __salt__['boto_transit_gw.create_vpc_attachment'](
                tg_id,
                vpc_id,
                subnet_ids,
                options,
                region,
                profile
            )
            if result:
                attachment_id = result['TransitGatewayAttachmentId']
                if __salt__['boto_transit_gw.wait_for_vpc_attachment'](
                    attachment_id,
                    region,
                    profile
                ):
                    ret['changes'] = {'old': None, 'new': result}
                    ret['comment'] = f'VPC Attachment {name} created.'
                else:
                    ret['result'] = False
                    ret['comment'] = f'VPC Attachment {name} did not become available.'
            else:
                ret['result'] = False
                ret['comment'] = f'Failed to create VPC Attachment {name}.'
        except ClientError as e:
            if e.response['Error']['Code'] == 'DuplicateTransitGatewayAttachment':
                ret['comment'] = f'VPC Attachment {name} already exists.'
            else:
                ret['result'] = False
                ret['comment'] = f'Failed to create VPC Attachment {name}: {e}'

    return ret

def vpc_attach_absent(
    attachment_id,
    region=None,
    profile=None
):
    """
    Ensure the specified VPC Attachment is absent.
    """
    ret = {'name': attachment_id, 'result': None, 'comment': '', 'changes': {}}
    if __opts__['test']:
        ret['comment'] = f'VPC Attachment {attachment_id} would be deleted.'
        ret['result'] = None
        return ret

    result = __salt__['boto_transit_gw.delete_vpc_attachment'](
        attachment_id,
        region,
        profile
    )
    if result:
        if __salt__['boto_transit_gw.wait_for_attachment_deletion'](
            attachment_id,
            region,
            profile
        ):
            ret['result'] = True
            ret['changes'] = {'old': attachment_id, 'new': None}
            ret['comment'] = f'VPC Attachment {attachment_id} deleted.'
        else:
            ret['result'] = False
            ret['comment'] = f'Failed to wait for VPC Attachment {attachment_id} deletion.'
    else:
        ret['result'] = False
        ret['comment'] = f'Failed to delete VPC Attachment {attachment_id}.'
    return ret

def transit_gw_absent(
    name,
    region=None,
    profile=None
):
    """
    Ensure the specified Transit Gateway is absent.
    """
    ret = {'name': name, 'result': True, 'comment': '', 'changes': {}}
    if __opts__['test']:
        ret['comment'] = f'Transit Gateway {name} would be deleted.'
        ret['result'] = None
        return ret

    tg_id = __salt__['boto_transit_gw.get_transit_gateway_id_by_name'](
        name,
        region,
        profile
    )
    if not tg_id:
        ret['comment'] = f'Transit Gateway {name} does not exist.'
        log.info(ret['comment'])
        return ret

    new_name = f"{name}-deleting"
    __salt__['boto_transit_gw.rename_transit_gateway'](
        tg_id,
        new_name,
        region,
        profile
    )

    attachments = __salt__['boto_transit_gw.describe_transit_gateway_vpc_attachments'](
        tg_id,
        region,
        profile
    )
    if attachments:
        for attachment in attachments:
            attachment_id = attachment['TransitGatewayAttachmentId']
            result = vpc_attach_absent(
                attachment_id,
                region,
                profile
            )
            if not result['result']:
                ret['result'] = False
                ret['comment'] = f'Failed to delete VPC attachment {attachment_id}.'
                return ret
            if not __salt__['boto_transit_gw.wait_for_attachment_deletion'](
                attachment_id,
                region,
                profile
            ):
                ret['result'] = False
                ret['comment'] = f'Failed to wait for VPC attachment {attachment_id} deletion.'
                return ret

    result = __salt__['boto_transit_gw.delete_transit_gateway'](
        tg_id,
        region,
        profile
    )
    if result:
        if __salt__['boto_transit_gw.wait_for_transit_gateway_deletion'](
            tg_id,
            region,
            profile
        ):
            ret['result'] = True
            ret['changes'] = {'old': tg_id, 'new': None}
            ret['comment'] = f'Transit Gateway {name} deleted.'
        else:
            ret['result'] = False
            ret['comment'] = f'Failed to wait for Transit Gateway {name} deletion.'
    else:
        ret['result'] = False
        ret['comment'] = f'Failed to delete Transit Gateway {name}.'
    return ret
