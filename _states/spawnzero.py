__virtualname__ = 'spawnzero'

def __virtual__():
    return __virtualname__

def check(name, type, value, **kwargs):
    """
    Check if spawnzero is complete by targeting spawning:0 of the specified role type,
    this pulls directly from mine data
    """

    ret = {"name": name, "result": False, "changes": {}, "comment": ""}

    expected = value

    if "test" not in kwargs:
        kwargs["test"] = __opts__.get("test", False)

    status = __salt__['mine.get'](tgt='G@role:'+type+' and G@spawning:0',tgt_type='compound',fun='spawnzero_complete')
    current = next(iter(status.values()))

    print(f'current spawnzero check value: {expected}')
    print(f'expected spawnzero check value: {expected}')

    if kwargs["test"]:
        if current == expected:
            ret["comment"] = "Spawnzero Check would be complete"
            ret["result"] = None
        else:
            ret["changes"] = {
                "old": current,
                "new": expected,
            }
            ret["comment"] = "Spawnzero Check would not be complete"
            ret["result"] = None
        return ret

    if current == expected:
        ret["result"] = True
        ret["comment"] = "Spawnzero Check Successful"
        return ret

    ret["changes"] = {
        "old": current,
        "new": expected,
    }
    ret["comment"] = "Spawnzero Check not complete"
    return ret