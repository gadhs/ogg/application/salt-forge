{% set type = opts.id.split('-')[0] %}
{% set role = opts.id.split('-')[0] %}

base:
  '*':
    - /formulas/common/base
  {{ type }}*:
    - /formulas/{{ role }}/configure