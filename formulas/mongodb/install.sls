include:
  - /formulas/common/base
  - /formulas/common/mongodb/repo

mongodb_pkgs:
  pkg.installed:
    - pkgs:
      - mongodb-enterprise
      - git
      - make
      - checkpolicy
      - policycoreutils
      - selinux-policy-devel
    - reload_modules: True