include:
  - /formulas/{{ grains['role'] }}/install

/etc/mongod.conf:
  file.managed:
    - source: salt://formulas/mongodb/files/mongod.conf

/etc/security/limits.d/90-ulimit-default.conf:
  file.managed:
    - source: salt://formulas/common/ulimit/90-ulimit-default.conf
    - user: root
    - group: root
    - mode: "0644"

mongod_service:
  service.running:
    - name: mongod
    - enable: true
    - watch:
      - file: /etc/mongod.conf

# mongod_selinux_config:
#   cmd.run:
#     - name: git config --system --add safe.directory "/var/lib/mongodb-selinux"
#     - unless:
#       - fun: grains.equals
#         key: build_phase
#         value: configure
#     - require:
#       - pkg: mongodb_pkgs

# mongodb-selinux_latest:
#   git.latest:
#     - name: https://github.com/mongodb/mongodb-selinux
#     - target: /var/lib/mongodb-selinux
#     - force_clone: true
#     - require:
#       - cmd: mongod_selinux_config

# mongodb-selinux-make:
#   cmd.run:
#     - name: make
#     - cwd: /var/lib/mongodb-selinux
#     - unless:
#       - fun: grains.equals
#         key: build_phase
#         value: configure
#     - require:
#       - git: mongodb-selinux_latest

# mongodb-selinux-install:
#   cmd.run:
#     - name: make install
#     - cwd: /var/lib/mongodb-selinux
#     - unless:
#       - fun: grains.equals
#         key: build_phase
#         value: configure
#     - require:
#       - cmd: mongodb-selinux-make