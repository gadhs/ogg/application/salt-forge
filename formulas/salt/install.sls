include:
  - /formulas/common/base

salt_pkgs:
  pkg.installed:
    - pkgs:
      - python3-tornado
      - salt-api
      - salt-cloud
      - haveged
      - python3-pygit2
      - patchelf
    - reload_modules: True

salt_pip:
  pip.installed:
    - bin_env: '/usr/bin/pip3'
    - reload_modules: True
    - names:
      - tornado
      - pygit2==1.13.1
      - requests
      - boto
      - boto3
      - botocore
      - kubernetes
    - require:
      - pkg: salt_pkgs

### temp patch for relenv bug https://github.com/saltstack/relenv/pull/183/files
{% for path in grains['pythonpath'] %}
  {% if 'site-packages' in path %}
    {% set site_packages_path = path %}

relenv_patch:
  file.managed:
    - name: {{ site_packages_path }}/relenv/runtime.py
    - source: salt://formulas/salt/files/runtime.py
  {% endif %}
{% endfor %}

salt-pip_installs:
  pip.installed:
    - bin_env: '/usr/bin/salt-pip'
    - reload_modules: true
    - names:
      - tornado
      - pygit2==1.13.1
      - requests
      - boto
      - boto3
      - botocore
      - kubernetes
    - require:
      - pkg: salt_pkgs
      - pip: salt_pip
      - file: relenv_patch
