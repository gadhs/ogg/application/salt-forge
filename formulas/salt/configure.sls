include:
  - /formulas/{{ grains['role'] }}/install

/srv/salt:
  file.directory:
    - makedirs: true

/srv/runners:
  file.directory:
    - makedirs: True

/srv/runners/needs.py:
  file.managed:
    - source: salt://_runners/needs.py

create_api_cert:
  cmd.run:
    - name: "salt-call --local tls.create_self_signed_cert"
    - creates:
      - /etc/pki/tls/certs/localhost.crt
      - /etc/pki/tls/certs/localhost.key

api:
  user.present:
    - password: {{ salt['pillar.get']('api:user_password', 'TBD') }}
    - hash_password: True
    - require:
      - file: /srv/dynamic_pillar/api.sls
      - file: /srv/dynamic_pillar/top.sls

/etc/salt/master.d/gitfs_pillar.conf:
  file.managed:
    - contents: |
        ext_pillar:
          - git:
            - {{ pillar['salt-forge_pillar_configuration']['branch'] }} {{ pillar['salt-forge_pillar_configuration']['url'] }}:
              - env: base
            {%- if salt['pillar.get']('salt-forge_pillar_configuration:token', False) %}
              - user: {{ pillar['salt-forge_pillar_configuration']['username'] }}
              - password: {{ pillar['salt-forge_pillar_configuration']['token'] }}
            {%- endif %}

        ext_pillar_first: False
        pillar_gitfs_ssl_verify: True

/etc/salt/master.d/gitfs_remotes.conf:
  file.managed:
    - contents: |
        gitfs_remotes:
          - {{ pillar['salt-forge_remote_configuration']['url'] }}:
            - saltenv:
              - base:
                - ref: {{ pillar['salt-forge_remote_configuration']['branch'] }}
            {%- if salt['pillar.get']('salt-forge_remote_configuration:token', False) %}
            - user: {{ pillar['salt-forge_remote_configuration']['username'] }}
            - password: {{ pillar['salt-forge_remote_configuration']['token'] }}
            {%- endif %}
{% for remote, config in pillar.get('gitfs_other_configurations', {}).items() %}
          - {{ pillar['gitfs_other_configurations'][remote]['url'] }}:
            - saltenv:
              - base:
                - ref: {{ pillar['gitfs_other_configurations'][remote]['branch'] }}
{% endfor %}
        gitfs_saltenv_whitelist:
          - base

{% for directive, contents in pillar.get('master-config', {}).items() %}
/etc/salt/master.d/{{ directive }}.conf:
  file.managed:
    - contents_pillar: master-config:{{ directive }}
{% endfor %}

/etc/salt/cloud.conf.d/minion.conf:
  file.managed:
    - contents: |
      minion:
        master: {{ pillar['salt']['record'] }}

### This is for salt-cloud
/etc/salt/cloud.providers.d/providers.conf:
  file.managed:
    - source: salt://formulas/salt/files/providers.conf
    - template: jinja
    - defaults:
        id: {{ pillar['cloud_providers']['aws']['aws_access_key_id'] }}
        key: {{ pillar['cloud_providers']['aws']['aws_secret_access_key'] }}
        location: {{ pillar['cloud_providers']['aws']['region_name'] }}
        master: {{ pillar['salt']['record'] }}

### Temporary patch for https://github.com/saltstack/salt-bootstrap/issues/2027
# /etc/salt/cloud.deploy.d/cust_bootstrap-salt.sh:
#   file.managed:
#     - source: salt://formulas/salt/files/bootstrap-salt.sh

### This is for using idem within salt

### Patch for msgpack util to handle datetime serialization
/opt/saltstack/salt/lib/python3.10/site-packages/salt/utils/msgpack.py:
  file.managed:
    - source: salt://formulas/salt/files/msgpack.py

/root/.aws/config:
  file.managed:
    - contents: |
        [default]
        aws_access_key_id: {{ pillar['cloud_providers']['aws']['aws_access_key_id'] }}
        aws_secret_access_key: {{ pillar['cloud_providers']['aws']['aws_secret_access_key'] }}
        region: {{ pillar['cloud_providers']['aws']['region_name'] }}

### This for using boto salt modules
/etc/salt/minion.d/aws.conf:
  file.managed:
    - contents: |
        aws:
          keyid: {{ pillar['cloud_providers']['aws']['aws_access_key_id'] }}
          key: {{ pillar['cloud_providers']['aws']['aws_secret_access_key'] }}
          region: {{ pillar['cloud_providers']['aws']['region_name'] }}

/srv/dynamic_pillar:
  file.directory

passwords:
  file.managed:
    - replace: false
    - names:
      - /srv/dynamic_pillar/api.sls:
        - contents: |
            api:
              user_password: {{ salt['random.get_str']('64', punctuation=False) }}
      - /srv/dynamic_pillar/cache.sls:
        - contents: |
            nexusproxy:
              nexusproxy_password: {{ salt['random.get_str']('16', punctuation=False) }}

/srv/dynamic_pillar/top.sls:
  file.managed:
    - source: salt://formulas/salt/files/top.sls
    - require:
      - file: /srv/dynamic_pillar/api.sls

/srv/dynamic_pillar/deps.sls:
  file.managed:
    - source: salt://formulas/salt/files/deps.sls

/etc/salt/master:
  file.managed:
    - contents: ''
    - contents_newline: False

salt-api_service:
  service.running:
    - name: salt-api
    - enable: True
    - watch:
      - file: /etc/salt/master
      - file: /etc/salt/master.d/*

build_phase_final:
  grains.present:
    - name: build_phase
    - value: configure

salt-master_watch:
  cmd.run:
    - name: 'salt-call service.restart salt-master'
    - bg: True
    - onchanges:
      - file: /etc/salt/master
      - file: /etc/salt/master.d/*
    - order: last
