### default dependencies file

hosts:
  mongodb:
    needs:
      configure:
        - salt
      available:
        - aws_vpc
  newhire:
    needs:
      configure:
        - salt
      available:
        - aws_vpc
        - aws_eks
