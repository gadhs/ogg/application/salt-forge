{% set type = opts.id.split('-')[0] %}
{% set role = opts.id.split('-')[0] %}

initial_module_sync:
  saltutil.sync_all:
    - refresh: True
    - unless:
      - fun: grains.has_value
        key: build_phase

build_phase:
  grains.present:
    - value: base
    - unless:
      - fun: grains.has_value
        key: build_phase

type:
  grains.present:
    - value: {{ type }}

role:
  grains.present:
    - value: {{ role }}

upload_epel_repo:
  file.managed:
    - name: /etc/yum.repos.d/repo-setup.sh
    - source: salt://formulas/common/epel/repo-setup.sh
    - user: root
    - group: root
    - mode: "0700"

configure_epel_script:
  cmd.script:
    - name: /etc/yum.repos.d/repo-setup.sh
    - shell: /bin/bash
    - user: root
    - require:
      - file: upload_epel_repo

base_install:
  pkg.installed:
    - pkgs:
      - python3-pip
    - reload_modules: True

{% for key in pillar['authorized_keys'] %}
{{ key }}:
  ssh_auth.present:
    - user: root
    - enc: {{ pillar['authorized_keys'][ key ]['encoding'] }}
{% endfor %}

salt-minion:
  service.enabled