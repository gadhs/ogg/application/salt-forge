### This macro looks up the VPC ID for a given VPC name
{%- macro lookup_vpc_id(vpc_name) -%}

{%- set vpc_info = salt['boto_vpc.describe'](vpc_name=vpc_name, profile='aws') -%}
{%- if vpc_info and vpc_info['vpc'] -%}
{%- set vpc = vpc_info['vpc'] -%}
{{ vpc['id'] }}
{%- else -%}
# VPC not found
{%- endif -%}

{%- endmacro -%}

### This macro looks up the Nat Gateway ID for a given VPC name
{%- macro lookup_nat_gw_id(vpc_name) -%}

{%- set nat_info = salt['boto_vpc.describe_nat_gateways'](vpc_name=vpc_name, profile='aws') -%}
{%- if nat_info and nat_info[0] -%}
{%- set nat_gateway = nat_info[0] -%}
{{ nat_gateway['NatGatewayId'] }}
{%- else -%}
# Nat Gateway not found
{%- endif -%}

{%- endmacro -%}

{%- macro lookup_peer_conn_id(conn_name) -%}

{%- set peer_info = salt['boto_vpc.describe_vpc_peering_connection'](conn_name, profile='aws') -%}
{%- if peer_info and peer_info[0] -%}
{%- set peer_conn = peer_info[0] -%}
{{ peer_conn['VPC-Peerings'] }}
{%- else -%}
# Peering Connection not found
{%- endif -%}

{%- endmacro -%}

### This macro looks up the Security Group ID for a given security group name
{%- macro lookup_secgroup_id(secgroup) -%}

{%- set secgroup_info = salt['boto_secgroup.get_group_id'](secgroup, profile='aws') -%}
{%- if secgroup_info -%}
{{ secgroup_info }}
{%- else -%}
# Security Group not found
{%- endif -%}

{%- endmacro -%}

### This macro looks up the Subnet ID for a given CIDR block
{%- macro lookup_subnet_id(cidr_block) -%}

{%- set subnet_info = salt['boto_vpc.describe_subnets'](cidr=cidr_block, profile='aws') -%}
{%- if subnet_info and subnet_info['subnets'] and subnet_info['subnets'][0] -%}
{%- set subnet = subnet_info['subnets'][0] -%}
{{ subnet['id'] }}
{%- else -%}
# Subnet not found
{%- endif -%}

{%- endmacro -%}

### This macro looks up the Transit Gateway ID for a given Transit Gateway name
{%- macro lookup_transit_gw_id(tg_name) -%}

{%- set tg_info = salt['boto_transit_gw.get_transit_gateway_id_by_name'](tg_name) -%}
{%- if tg_info -%}
{{ tg_info }}
{%- else -%}
# Transit Gateway not found
{%- endif -%}

{%- endmacro -%}

### This macro looks up the Role ARN for a given role name
{%- macro lookup_role_arn(role_name) -%}

{%- set role_info = salt['boto_iam.describe_role'](role_name, profile='aws') -%}
{%- if role_info and role_info['arn'] -%}
{{ role_info['arn'] }}
{%- else -%}
# Role not found
{%- endif -%}

{%- endmacro -%}
