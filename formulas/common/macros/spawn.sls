{% macro spawnzero_complete() %}
spawnzero_complete:
  grains.present:
    - value: True

spawnzero_update:
  module.run:
    - mine.update:
    - require:
      - spawnzero_complete
    - unless:
      - fun: grains.equals
        key: build_phase
        value: configure
{% endmacro %}

{% macro check_spawnzero_status(type) %}
check_spawnzero_status:
  spawnzero.check:
    - name: check_spawnzero_status
    - type: {{ type }}
    - value: True
    - retry:
        attempts: 20
        interval: 30
    - unless:
      - fun: grains.equals
        key: build_phase
        value: configure

{% endmacro %}