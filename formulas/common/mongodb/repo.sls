mongodb-enterprise_repo:
  pkgrepo.managed:
    - humanname: MongoDB Enterprise Repository
    - baseurl: https://repo.mongodb.com/yum/redhat/9/mongodb-enterprise/7.0/$basearch/
    - gpgcheck: 1
    - enabled: 1
    - file: /etc/yum.repos.d/mongodb-enterprise-7.0.repo
    - gpgkey: https://pgp.mongodb.com/server-7.0.asc

update_packages_mongodb:
  pkg.uptodate:
    - refresh: true
    - onchanges:
      - mongodb-enterprise_repo
