docker_ce_repo:
  pkgrepo.managed:
    - humanname: Docker CE Stable - $basearch
    - baseurl: https://download.docker.com/linux/centos/$releasever/$basearch/stable
    - gpgcheck: 1
    - enabled: 1
    - file: /etc/yum.repos.d/docker-ce.repo
    - gpgkey: https://download.docker.com/linux/centos/gpg

update_packages_mongodb:
  pkg.uptodate:
    - refresh: true
    - onchanges:
      - docker_ce_repo