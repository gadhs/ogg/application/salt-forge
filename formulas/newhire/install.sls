include:
  - /formulas/common/base
  - /formulas/common/docker/repo

newhire_packages:
  pkg.installed:
    - pkgs:
      - python3-pip
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-buildx-plugin
      - docker-compose-plugin
    - reload_modules: True

salt-pip_installs:
  pip.installed:
    - bin_env: '/usr/bin/salt-pip'
    - reload_modules: true
    - pkgs:
      - docker
    - require:
      - pkg: newhire_packages