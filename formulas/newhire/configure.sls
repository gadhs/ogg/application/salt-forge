include:
  - /formulas/{{ grains['role'] }}/install

docker_login:
  cmd.run:
    - name: docker login {{ pillar['gitlab_registry']['url'] }} --username {{ pillar['gitlab_registry']['user'] }} --password {{ pillar['gitlab_registry']['token'] }}

docker_image_pull:
  cmd.run:
    - name: docker pull {{ pillar['gitlab_registry']['url'] }}:{{ pillar['gitlab_registry']['tag'] }}
    - require:
      - cmd: docker_login
    - unless:
      - docker image list | grep -q {{ pillar['gitlab_registry']['url'] }}

newhire:
  docker_container.running:
    - name: demo-app
    - image: {{ pillar['gitlab_registry']['url'] }}:{{ pillar['gitlab_registry']['tag'] }}
    - restart_policy: unless-stopped
    - ports:
      - 8080
    - port_bindings:
      - {{ pillar['gitlab_registry']['port'] }}:8080
    - require:
      - cmd: docker_image_pull
      - service: docker_service

docker_logout:
  cmd.run:
    - name: docker logout

docker_service:
  service.running:
    - name: docker
    - enable: True